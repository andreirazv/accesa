~~~~~~GAME~~~~~~~
POST /api/game - adauga un joc
{
"gameName": "Fifa18",
"gameRules": "google",
"countPlayed": 41
}
GET /api/game - afiseaza toate jocurile
GET /api/game/GameName - afiseaza un joc dupa nume
GET /api/game/desc - Returneaza toate jocurile ordonate descrescator dupa nr de cate ori a fost jucat
DELETE  /api/game/GameName - sterge un joc dupa nume
~~~~~~~~~~~~~~~~~~~~


~~~~~~Player~~~~~~~~
GET /api/player - afiseaza toti jucatori
GET /api/player/PlayerName - afiseaza jucatorul cu numele PlayerName
GET /api/player - afiseaza toti jucatori ordonati dupa data de inregistrare
POST /api/player - adauga un jucator
{
    "playerName": "NewPlayer",
    "passwordPin": "81dc9bdb52d04dc20036dbd8313ed055"
}
DELETE  /api/result/player/PlayerName - sterge jucator-ul cu numele PlayerName
~~~~~~~~~~~~~~~~~~~


~~~~~~MATCH~~~~~~~~
GET /api/match - afiseaza toate meciurile
GET /api/match/GameName - afiseaza toate jocurile dupa nume din colectia match 
POST /api/match - adauga un meci
{
}
DELETE  /api/game/IdMatch - sterge un joc dupa id
~~~~~~~~~~~~~~~~~~~


~~~~~~Team~~~~~~~~
GET /api/team - afiseaza toate echipele
GET /api/team/TeamName - afiseaza echipa cu numele TeamName 
GET /api/team/desc - afiseaza echipele in ordine descrescatore dupa numarul de playeri din echipa
POST /api/team - adauga o echipa
{
    "TeamName" : "Awesome",
    "Players" : [ 
    ]
}
DELETE  /api/team/TeamName - sterge o echipa dupa nume
~~~~~~~~~~~~~~~~~~~


~~~~~~Result~~~~~~~~
GET /api/result - afiseaza toate rezultatele
GET /api/result/GameName - afiseaza toate rezultatele dupa numele unui joc 
POST /api/result - adauga un rezultat
{

}
DELETE  /api/result/IdStats - sterge un rezultat dupa id
~~~~~~~~~~~~~~~~~~~


~~~~~~GameType~~~~~~~~
GET /api/type - afiseaza toate tipurile de joc
GET /api/type/GameName - afiseaza tipul de joc care are numele GameName
GET /api/type/desc - Returneaza toate tipurile de joc ordonate descrescator dupa nr de cate ori a fost jucat
POST /api/type - adauga un tip de joc
{
    "typeName": "Fifa15",
    "onlyPlayers": true,
    "onlyTeams": false,
    "timeToPlay": 90,
    "rounds": 2,
    "timePerRound": 0,
    "pointsRemaining": 0,
    "maxScore": 0,
    "maxTeams": 0,
    "maxPlayers": 2,
    "allowDraw": true,
    "startPoints": 0,
    "countPlayed": 6
}
DELETE  /api/result/type/TypeName - sterge un tip de joc dupa nume
~~~~~~~~~~~~~~~~~~~


~~~~~~Photo~~~~~~~~
GET /api/photo - afiseaza toate pozele
POST /api/photo - adauga o poza
{
	"sourcePhoto": "/Photos/Players/NewPlayer.png"
}
DELETE  /api/result/photo/IdPhoto - sterge o poza dupa id
~~~~~~~~~~~~~~~~~~~




