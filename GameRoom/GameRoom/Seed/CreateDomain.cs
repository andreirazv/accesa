﻿using System;
using System.Collections.Generic;
using System.Text;
using GameRoom.Domain;
using GameRoom.Utils;
using MongoDB.Bson;
using MongoDB.Driver;

namespace GameRoom.Seed
{
    class CreateDomain
    {

        public Photo CreatePhoto(IMongoCollection<Photo> ColPhoto, string source)
        {
            Photo Photo1 = new Photo
            {
                _id = ObjectId.GenerateNewId(),
                SourcePhoto = source
            };
            var Builder = Builders<Photo>.Filter;
            var Filter = Builder.Eq(u => u.SourcePhoto, source);
            if (ColPhoto.Find(Filter).ToList().Count == 0)
            {
                ColPhoto.InsertOne(Photo1);
            }
            return Photo1;
        }
        public Game CreateGame(IMongoCollection<Game> ColGame, string NameGame, string Rules, int Count, ObjectId PhotoId)
        {
            Game Game1 = new Game
            {
                _id = ObjectId.GenerateNewId(),
                GameName = NameGame,
                GameRules = Rules,
                CountPlayed = Count,
                GamePhotoId = PhotoId.ToString()
            };
            var Builder = Builders<Game>.Filter;
            var Filter = Builder.Eq(u => u.GameName, NameGame);
            if (ColGame.Find(Filter).ToList().Count == 0)
            {    
                ColGame.InsertOne(Game1);              
            }
            return Game1;
        }

        public Player CreatePlayer(IMongoCollection<Player> ColPlayer, string NamePlayer, string username,string Pass, ObjectId StatPlayer, ObjectId PhotoId, DateTime date)
        {
            Player ReturnPlayer = new Player
            {
                _id = ObjectId.GenerateNewId(),
                PlayerName = NamePlayer,
                Password = Pass,
                Username = username,
                TotalStatsId = StatPlayer.ToString(),
                PlayerPhotoId = PhotoId.ToString(),
                RegisterDate = date
            };
            var Builder = Builders<Player>.Filter;
            var Filter = Builder.Eq(u => u.PlayerName, NamePlayer);
            if (ColPlayer.Find(Filter).ToList().Count == 0)
            {
                ColPlayer.InsertOne(ReturnPlayer);
            }
            return ReturnPlayer;
        }
        public Stats CreateStat(IMongoCollection<Stats> ColStat, int Win, int Defeat, int Draw)
        {
            Stats Stat1 = new Stats
            {
                _id = ObjectId.GenerateNewId(),
                Wins = Win,
                Deafeats = Defeat,
                Draws = Draw
            };
            ColStat.InsertOne(Stat1);
            return Stat1;

        }
        public Team CreateTeam(IMongoCollection<Team> ColTeam, string Name, List<string> ListPlayer, ObjectId photoId)
        {
            Team Team1 = new Team
            {
                _id = ObjectId.GenerateNewId(),
                TeamName = Name,
                Players = ListPlayer,
                PhotoId = photoId.ToString()
            };
            var Builder = Builders<Team>.Filter;
            var Filter = Builder.Eq(u => u.TeamName, Name);
            if (ColTeam.Find(Filter).ToList().Count == 0)
            {
                ColTeam.InsertOne(Team1); 
            }
            return Team1;
        }

        public GameType CreateGameType(IMongoCollection<GameType> ColType,string Name,int remPoints,int RoundsNumber,int Count,bool allowteam,string photoId)
        {
            GameType GameType1 = new GameType
            {
                _id = ObjectId.GenerateNewId(),
                TypeName = Name,
                Rounds = RoundsNumber,
                RemainingPoints = remPoints,
                AllowTeam = allowteam,
                CountPlayed = Count,
                PhotoId = photoId
            };
            var Builder = Builders<GameType>.Filter;
            var Filter = Builder.Eq(u => u.TypeName, Name);
            if (ColType.Find(Filter).ToList().Count == 0)
            {
                ColType.InsertOne(GameType1);
            }
            return GameType1;
        }
        public Match CreateMatch(IMongoCollection<Match> ColMatch, ObjectId IdGame, ObjectId IdTypeGame, List<string> TeamsIdList,string winner,string photo,DateTime date)
        {

            Dictionary<string, double> Dict = new Dictionary<string, double>();
            Random rnd = new Random();
            TeamsIdList.ForEach((string id) =>
            {
                int value = rnd.Next(1, 10);
                Dict.Add(id, value);
            });

            Match Match1 = new Match
            {
                _id = ObjectId.GenerateNewId(),
                GameId = IdGame.ToString(),
                TypeGameId = IdTypeGame.ToString(),
                TeamsAndPoints = Dict,
                WinnerTeam = winner,
                FinalPhotoId = photo,
                DateEndMatch = date
            };
            ColMatch.InsertOne(Match1);
            return Match1;
        }
        public Result CreateResult(IMongoCollection<Result> ColResult, ObjectId IdTeam, ObjectId IdGame, ObjectId IdTypeGame, ObjectId IdStat, DateTime DateParam)
        {

            Result Result1 = new Result
            {
                _id = ObjectId.GenerateNewId(),
                TeamId = IdTeam.ToString(),
                GameId = IdGame.ToString(),
                TypeGameId = IdTypeGame.ToString(),
                StatsId = IdStat.ToString(),
                Date = DateParam
            };
            ColResult.InsertOne(Result1);
            return Result1;
        }
    }
}

        
    
