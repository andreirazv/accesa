﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace GameRoom.Domain
{
    class Photo
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public string SourcePhoto { get; set; }
        public override string ToString()
        {
            return
                "IdPhoto: " + _id + "\n" +
                "sourcePhoto: " + SourcePhoto + "\n";
        }
    }
}
