﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
namespace GameRoom.Domain
{
    class Game
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public string GameName { get; set; }
        public string GameRules { get; set; }
        public int CountPlayed { get; set; }
        public string GamePhotoId { get; set; }


        public override string ToString()
        {
            return
                "IdGame: " + _id + "\n"+
                "GameName: " + GameName + "\n" +
                "GameRules: " + GameRules + "\n" +
                "CountPlayed: " + CountPlayed + "\n" +
                "GamePhotoId: " + GamePhotoId + "\n";
        }
    }
}
