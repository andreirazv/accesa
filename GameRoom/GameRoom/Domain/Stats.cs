﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
namespace GameRoom.Domain
{
    class Stats
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public int Wins { get; set; }
        public int Deafeats { get; set; }
        public int Draws { get; set; }
        public override string ToString()
        {
            return
                "IdStats: " + _id + "\n" +
                "Wins: " + Wins + "\n" +
                "Deafeats: " + Deafeats + "\n" +
                "Draws: " + Draws + "\n";
        }
    }
}
