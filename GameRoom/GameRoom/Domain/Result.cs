﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;

namespace GameRoom.Domain
{
    class Result
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public string TeamId { get; set; }
        public string GameId { get; set; }
        public string TypeGameId { get; set; }
        public string StatsId { get; set; }
        public DateTime Date { get; set; }
        public override string ToString()
        {
            return
                "IdResult: " + _id + "\n" +
                "TeamId: " + TeamId + "\n" +
                "GameId: " + GameId + "\n" +
                "TypeGameId: " + TypeGameId + "\n" +
                "StatsId: " + StatsId + "\n" +
                "Date: " + Date + "\n";
        }

    }
}
