﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GameRoom.Domain
{
    class GameType
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public string TypeName { get; set; }
        public bool AllowTeam { get; set; }
        public int RemainingPoints { get; set; }
        public int Time { get; set; }
        public int Rounds { get; set; }
        public int CountPlayed { get; set; }
        public string PhotoId { get; set; }

        public override string ToString()
        {
            return
                "IdGameType: " + _id + "\n" +
                "TypeName: " + TypeName + "\n" +
                "AllowTeam: " + AllowTeam + "\n" +
                "RemainingPoints: " + RemainingPoints + "\n" +
                "Rounds: " + Rounds + "\n" +
                "Time: " + Time + "\n" +
                "CountPlayed: " + CountPlayed + "\n"+
                "PhotoId: " + PhotoId + "\n";
        }

    }
}
