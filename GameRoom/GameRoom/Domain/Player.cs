﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;

namespace GameRoom.Domain
{
    class Player
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public string PlayerName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string TotalStatsId { get; set; }
        public string PlayerPhotoId { get; set; }
        public DateTime RegisterDate { get; set; }
        public override string ToString()
        {
            return
                "IdPlayer: " + _id + "\n" +
                "PlayerName: " + PlayerName + "\n" +
                "Username: " + Username + "\n" +
                "Password: " + Password + "\n" +
                "TotalStatsId: " + TotalStatsId + "\n" +
                "RegisterDate: " + RegisterDate + "\n" +
                "PlayerPhotoId: " + PlayerPhotoId + "\n";
        }
    }
}
