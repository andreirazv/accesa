﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;

namespace GameRoom.Domain
{
    class Match
    {
        [BsonId]
        public ObjectId _id { get; set; }      
        public string GameId { get; set; }
        public string TypeGameId { get; set; }
        [BsonDictionaryOptions(DictionaryRepresentation.ArrayOfArrays)]
        public Dictionary<string, double> TeamsAndPoints { get; set; }
        public string WinnerTeam { get; set; }
        public string FinalPhotoId { get; set; }
        public DateTime DateEndMatch { get; set; }

        public override string ToString()
        {
            return
                "IdMatch: " + _id + "\n" +
                "GameId: " + GameId + "\n" +
                "TypeGameId: " + TypeGameId + "\n" +
                "TeamsAndPoints: " + string.Join(";", TeamsAndPoints.Select(x => x.Key + "=" + x.Value).ToArray()) + "\n" +
                "WinnerTeam: " + WinnerTeam + "\n"+
                "FinalPhotoId: " + FinalPhotoId + "\n"+
                "DateEndMatch: " + DateEndMatch + "\n";
        }
    }
}
