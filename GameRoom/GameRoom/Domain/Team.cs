﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;

namespace GameRoom.Domain
{
    class Team
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public string TeamName { get; set; }
        public List<string> Players { get; set; }
        public string PhotoId { get; set; }

        public override string ToString()
        {
            return
                "IdTeam: " + _id + "\n" +
                "TeamName: " + TeamName + "\n" +
                "Players: " + string.Join(";", Players) + "\n"+
                "PhotoId: " + PhotoId + "\n";
        }
    }
}
