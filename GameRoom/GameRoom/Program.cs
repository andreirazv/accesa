﻿using MongoDB.Driver;
using System;
using GameRoom.Domain;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using MongoDB.Bson;
using GameRoom.Seed;

namespace GameRoom
{
    class Program
    {
        
        static void Main(string[] args)
        {
            try
            {
                MongoClient Client = new MongoClient();
                SeedDb seed = new SeedDb();
                IMongoDatabase MongoDatabase = Client.GetDatabase("gameroom");
                seed.SeedTheDb(MongoDatabase);
                Console.ReadLine();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadLine();
            }
        }
    }
}
