import * as React from 'react';
import './App.css';
import Header from './components/Header/Header';
import Root from './Root';
import AuthService from './services/AuthService';
export interface IAppState {
	Auth: AuthService;
	user:any;
	startTimeMS :number;  
	timerStep:number
	time:number
	interval:any
}
export default class App extends React.Component<any,IAppState>
{
	constructor(props:any) {
        super(props);
        this.state = {
            user: null,
			Auth : new AuthService(),
			startTimeMS : 0,  
			timerStep:3000,
			time:0,
			interval:0
		}
        this.checkLogged = this.checkLogged.bind(this)
    }
	componentWillMount() {

		this.startTimer();

		if (!this.state.Auth.loggedIn()) {
		}
		else {
			try {
				const profile = this.state.Auth.getProfile()
				this.setState({
					user: profile
				})
				
			}
			catch(err){
				this.state.Auth.logout();
				
			}
		}
}
startTimer(){
	if(!this.state.Auth.loggedIn()&&location.pathname!='/'){
	this.setState({
		startTimeMS : (new Date()).getTime(),
	});
}
 }
 getRemainingTime(){
    return ((this.state.timerStep - ( new Date().getTime() - this.state.startTimeMS ))/1000).toFixed(0);
}
public checkLogged=()=>
{
	if(!this.state.Auth.loggedIn()&&location.pathname!='/')
	{
		window.setTimeout(function(){
			
			history.pushState('','','/');
			history.go();
	
		}, this.state.timerStep);
		return true;
	}
	return false;
}
	componentDidMount() {
		if(!this.state.Auth.loggedIn()&&location.pathname!='/')
		{this.setState({
			interval : setInterval(() => this.setState({ time: Date.now() }), 1000)
		});}
  	}
  componentWillUnmount() {

	if(!this.state.Auth.loggedIn()&&location.pathname!='/'){
	clearInterval(this.state.interval);
	}
  }
	public render(){

		return (
			<div>
				{!!this.checkLogged()&&
					<div>
						<h1>You should log in first!</h1>
						<h2>You will be redirected to first page in {this.getRemainingTime()} sec!</h2>
						</div>
				}
				{!!!this.checkLogged()&&
				<Header/>}
				{!!!this.checkLogged()&&
				<Root />
				}
			</div>
			
		)
	}
}
