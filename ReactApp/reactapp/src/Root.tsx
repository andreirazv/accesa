import { Switch, Route } from "react-router-dom";
import * as React from 'react';
import RankPanel from './components/RankPanel/RankPanel';
import PhotoManager from './components/ManagePhoto/PhotoManager';
import UpdateGame from './components/ManageGames/UpdateGame';
import UpdatePhoto from './components/ManagePhoto/UpdatePhoto';
import UpdateGameType from './components/ManageGameType/UpdateGameType';
import GameTypeManager from './components/ManageGameType/GameTypeManager';
import GamePlay from './components/GamePlay/GamePlay';
import PlayerManager from './components/ManagePlayers/PlayerManager';
import UpdatePlayer from './components/ManagePlayers/UpdatePlayer';
import MatchManager from './components/ManageMatch/MatchManager';
import UpdateMatch from './components/ManageMatch/UpdateMatch';
import Home from './components/Home/Home';
import GameTypeList from './components/GamePlay/TypeGameManager/GameTypeList';
import HandlerTeam from './components/GamePlay/ManageTeam/HandlerTeam';
import HandlerBattle from './components/GamePlay/Battle/HandlerBattle';
import ImageDetection from 'src/components/ImageDetection/ImageDetection';
import FinishedBattle from './components/GamePlay/Battle/FinishedBattle';
import HandleCategory from './components/ImageDetection/HandleCategory';
import ViewCategory from './components/ImageDetection/ViewCategory';
import AddGame from './components/ManageGames/AddGame';

const Root = () => (
    <main>
        <Switch>
           
         
          <Route exact={true} path="/" component={Home} />
          <Route path="/leaderboard" component={RankPanel} />
          <Route path="/manage/game/:id" component={UpdateGame} />
          <Route path="/add/game" component={AddGame}/>
          <Route path="/manage/photo/:id" component={UpdatePhoto} />
          <Route path="/manage/photo" component={PhotoManager}/>
          <Route path="/manage/gametype/:id" component={UpdateGameType} />
          <Route path="/manage/gametype" component={GameTypeManager}/>
          <Route path="/manage/player/:id" component={UpdatePlayer} />
          <Route path="/manage/player" component={PlayerManager}/>
          <Route path="/manage/match/:id" component={UpdateMatch} />
          <Route path="/manage/match" component={MatchManager}/>
          <Route path="/select/game" component={GamePlay}/>
          <Route path="/select/type?game=:game" component={GameTypeList}/>
          <Route path="/select/type" component={GameTypeList}/>
          <Route path="/select/team?game=:game&type=:type" component={HandlerTeam}/>
          <Route path="/select/team" component={HandlerTeam}/>
          <Route path="/img-category" component={HandleCategory}/>
          <Route path="/view-only?category=:category" component={ViewCategory}/>
          <Route path="/view-only" component={ViewCategory}/>
          <Route path="/play?game=:game&type=:type&foreach=:foreach&teams=:teams" component={HandlerBattle}/>
          <Route path="/play?game=:game&type=:type&foreach=:foreach&teams=:teams&match=:match" component={HandlerBattle}/>
          <Route path="/play?game=:game&type=:type&foreach=:foreach&teams=:teams&match=:match&winnerTeam=:winnerTeam" component={HandlerBattle}/>
          <Route path="/play/finished?match=:match&finished=:finished" component={FinishedBattle}/>
          <Route path="/play/finished" component={FinishedBattle}/>
          <Route path="/play" component={HandlerBattle}/>
          <Route path="/recognizeimage" component={ImageDetection}/>
        </Switch>
    </main>
)
export default Root;