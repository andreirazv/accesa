import axios, { AxiosResponse } from 'axios';
import { IPhoto } from 'src/DataModel/IPhoto';
import { config } from 'src/config/config';
import { IRequest } from 'src/DataModel/IRequest';

export default class PhotosService {

    public static getAllPhotos = (): Promise<IPhoto[]> => {
        return axios
                .get(`${config.apiUrl}/photo`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getPhotoById = (photoId: string): Promise<IPhoto> => {
        return axios
                .get(`${config.apiUrl}/photo/id/${photoId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static deletePhoto = (photoId: string): Promise<any>  => {
        return axios
                .delete(`${config.apiUrl}/photo/${photoId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static insertPhoto = (photo: IPhoto): Promise<IPhoto> => {
        return axios
                .post(`${config.apiUrl}/photo`, photo)
                .then((result: AxiosResponse) => result.data);
    }
    public static recognize = (photo: IRequest): Promise<string> => {
        return axios
                .post(`${config.apiUrl}/photo/img64`, photo)
                .then((result: AxiosResponse) => result.data);
    }
 
    public static updatePhoto = (photoId: string, photo: IPhoto): Promise<IPhoto> => {
        return axios
                .put(`${config.apiUrl}/photo/${photoId}`, photo)
                .then((result: AxiosResponse) => result.data)
    }
}