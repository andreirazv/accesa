import axios, { AxiosResponse } from 'axios';
import { IPlayer } from 'src/DataModel/IPlayer';
import { config } from 'src/config/config';

export default class PlayerService {

    public static getAllPlayers = (): Promise<IPlayer[]> => {
        return axios
                .get(`${config.apiUrl}/player`, config.restDbconfig)
                .then((result: AxiosResponse) => result.data);
    }
    public static getPlayerById = (playerId: string): Promise<IPlayer> => {
        return axios
                .get(`${config.apiUrl}/player/id/${playerId}`)
                .then((result: AxiosResponse) => result.data);
    }
    public static getPlayerByName = (playerName: string): Promise<IPlayer> => {
        return axios
                .get(`${config.apiUrl}/player/name/${playerName}`)
                .then((result: AxiosResponse) => result.data);
    }
    public static deletePlayer = (playerId: string): Promise<any>  => {
        return axios
                .delete(`${config.apiUrl}/player/${playerId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getAllScorePlayers = (player: IPlayer): Promise<IPlayer> => {
        return axios
                .post(`${config.apiUrl}/player`, player)
                .then((result: AxiosResponse) => result.data);
    }

    public static insertPlayer = (player: IPlayer): Promise<IPlayer> => {
        return axios
                .post(`${config.apiUrl}/player`, player)
                .then((result: AxiosResponse) => result.data);
    }

    public static updatePlayer = (playerId: string, player: IPlayer): Promise<IPlayer> => {
        return axios
                .put(`${config.apiUrl}/player/${playerId}`, player)
                .then((result: AxiosResponse) => result.data)
    }
}