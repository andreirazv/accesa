import axios, { AxiosResponse } from 'axios';
import { config } from 'src/config/config';
import { ITeam } from 'src/DataModel/ITeam';

export default class TeamService {

    public static getAllTeams = (): Promise<ITeam[]> => {
        return axios
                .get(`${config.apiUrl}/team`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getTeamById = (teamId: String): Promise<ITeam> => {
        return axios
                .get(`${config.apiUrl}/team/id/${teamId}`)
                .then((result: AxiosResponse) => result.data);
    }
    public static getTeamByPlayerId = (playerId: string): Promise<ITeam> => {
        return axios
                .get(`${config.apiUrl}/team/playerid=${playerId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static deleteTeam = (teamId: string): Promise<any>  => {
        return axios
                .delete(`${config.apiUrl}/team/${teamId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static insertTeam = (team: ITeam): Promise<ITeam> => {
        return axios
                .post(`${config.apiUrl}/team`, team)
                .then((result: AxiosResponse) => result.data);
    }

    public static updateTeam = (teamId: string, team: ITeam): Promise<ITeam> => {
        return axios
                .put(`${config.apiUrl}/team/${teamId}`, team)
                .then((result: AxiosResponse) => result.data)
    }
}