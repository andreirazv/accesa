import decode from 'jwt-decode';
import { config } from 'src/config/config';
import { IPlayer } from 'src/DataModel/IPlayer';
import PlayerService from './PlayerService';
interface TokenDto {
    foo: string;
    exp: number;
    iat: number;
  }

export default class AuthService {
    constructor() {
        this.fetch = this.fetch.bind(this) 
        this.login = this.login.bind(this)
        this.getProfile = this.getProfile.bind(this)
    }
    getPlayerLogged=():Promise<IPlayer>=>
    {
        let playerId = localStorage.getItem("logged_playerId");
       return PlayerService.getPlayerById(playerId||'');
    
    }
    login = (username:string, password:string): Promise<IPlayer> => {

        return this
            .fetch(`${config.apiUrl}/auth/authenticate`, {
                method: 'POST',
                body: JSON.stringify({
                    username,
                    password
                })
                
            }).then((result: IPlayer) => 
                       result
            );
    }
   loggedIn() {
        
        const token = this.getToken()
        return !!token && !this.isTokenExpired(token) 
    }

    isTokenExpired(token:string) {
        try {
            
            const decoded = decode<TokenDto>(token);
            if (decoded.exp < Date.now() / 1000) { 
                return true;
            }
            else
                return false;
        }
        catch (err) {
            return false;
        }
    }

    setToken(idToken:string) {
        localStorage.setItem('id_token', idToken)
    }

    getToken() {
        return localStorage.getItem('id_token')
    }

    logout() {
        localStorage.removeItem('id_token');
    }

   getProfile() {
        return decode(this.getToken()||'');
    }


    fetch(url:string, options:any) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(response => response.json())
    }

    _checkStatus(response:any) {
        if (response.status >= 200 && response.status < 300) { 
            return response
        } else {
            var error = new Error(response.statusText)
            error.message = response
            throw error
        }
    }
}