import axios, { AxiosResponse } from 'axios';
import { IGame } from 'src/DataModel/IGame';
import { config } from 'src/config/config';

export default class GamesService {

    public static getAllGames = (): Promise<IGame[]> => {
        return axios
                .get(`${config.apiUrl}/game`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getGameById = (gameId: string): Promise<IGame> => {
        return axios
                .get(`${config.apiUrl}/game/id/${gameId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static deleteGame = (gameId: string): Promise<any>  => {
        return axios
                .delete(`${config.apiUrl}/game/${gameId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static insertGame = (game: IGame): Promise<IGame> => {
        return axios
                .post(`${config.apiUrl}/game`, game)
                .then((result: AxiosResponse) => result.data);
    }

    public static updateGame = (gameId: string, game: IGame): Promise<IGame> => {
        return axios
                .put(`${config.apiUrl}/game/${gameId}`, game)
                .then((result: AxiosResponse) => result.data)
    }
}