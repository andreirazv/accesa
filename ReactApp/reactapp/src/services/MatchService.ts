import axios, { AxiosResponse } from 'axios';
import { config } from 'src/config/config';
import { IMatch } from 'src/DataModel/IMatch';

export default class MatchService {

    public static getAllMatchs = (): Promise<IMatch[]> => {
        return axios
                .get(`${config.apiUrl}/match`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getWinner = (matchId: string): Promise<string> => {
        return axios
                .get(`${config.apiUrl}/match/getwinner&match=${matchId}`)
                .then((result: AxiosResponse) => result.data);
    }
    public static getMatchById= (matchId: string): Promise<IMatch> => {
        return axios
                .get(`${config.apiUrl}/match/${matchId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static deleteMatch = (matchId: string): Promise<any>  => {
        return axios
                .delete(`${config.apiUrl}/match/${matchId}`)
                .then((result: AxiosResponse) => result.data);
    }
    public static addTeamToMatch = (matchId: string,teamid: string): Promise<IMatch>  => {
        return axios
                .get(`${config.apiUrl}/match/add&matchid=${matchId}&teamid=${teamid}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static insertMatch = (match: IMatch): Promise<IMatch> => {
        return axios
                .post(`${config.apiUrl}/match`, match)
                .then((result: AxiosResponse) => result.data);
    }
    public static getTeamPoints = (matchid: string,teamid:string): Promise<number> => {
        return axios
                .get(`${config.apiUrl}/match/getpoints&matchid=${matchid}&teamid=${teamid}`)
                .then((result: AxiosResponse) => result.data);
    }
    public static addPoints = (matchid: string,teamid:string,points:string): Promise<IMatch> => {
        return axios
                .get(`${config.apiUrl}/match/matchid=${matchid}&teamid=${teamid}&points=${points}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static updateMatch = (matchId: string, match: IMatch): Promise<IMatch> => {
        return axios
                .put(`${config.apiUrl}/match/${matchId}`, match)
                .then((result: AxiosResponse) => result.data)
    }
}