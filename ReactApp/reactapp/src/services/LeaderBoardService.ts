import axios, { AxiosResponse } from 'axios';
import { config } from 'src/config/config';
import { ILeaderBoard } from 'src/DataModel/ILeaderBoard';

export default class LeaderBoardService {
    public static getPlayersStatus = (): Promise<ILeaderBoard[]> => {
        return axios
                .get(`${config.apiUrl}/player/rank`)
                .then((result: AxiosResponse) => result.data);
    }
    public static getPaggData = (query:string): Promise<ILeaderBoard[]> => {
        return axios
                .get(`${query}`)
                .then((result: AxiosResponse) => result.data);
    }

}