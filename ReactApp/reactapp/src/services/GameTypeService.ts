import axios, { AxiosResponse } from 'axios';
import { config } from 'src/config/config';
import { IGameType } from 'src/DataModel/IGameType';

export default class GameTypeService {

    public static getAllGameTypes = (): Promise<IGameType[]> => {
        return axios
                .get(`${config.apiUrl}/type`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getGameTypeById = (gameTypeId: string): Promise<IGameType> => {
        return axios
                .get(`${config.apiUrl}/type/id/${gameTypeId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static deleteGameType = (gameTypeId: string): Promise<any>  => {
        return axios
                .delete(`${config.apiUrl}/type/${gameTypeId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static insertGameType = (gameType: IGameType): Promise<IGameType> => {
        return axios
                .post(`${config.apiUrl}/type`, gameType)
                .then((result: AxiosResponse) => result.data);
    }

    public static updateGameType = (gameTypeId: string, gameType: IGameType): Promise<IGameType> => {
        return axios
                .put(`${config.apiUrl}/type/${gameTypeId}`, gameType)
                .then((result: AxiosResponse) => result.data)
    }
}