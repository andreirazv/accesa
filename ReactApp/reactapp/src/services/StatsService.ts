import axios, { AxiosResponse } from 'axios';
import { IStatus } from 'src/DataModel/IStatus';
import { config } from 'src/config/config';


export default class StatsService {

    public static getAllStats = (): Promise<IStatus[]> => {
        return axios
                .get(`${config.apiUrl}/stats`)
                .then((result: AxiosResponse) => result.data);
    }

    public static getStatById = (statsId: string): Promise<IStatus> => {
        return axios
                .get(`${config.apiUrl}/stats/id/${statsId}`)
                .then((result: AxiosResponse) => result.data);
    }
    public static getStatsForPlayer = (gamdId: string,typeId: string): Promise<IStatus> => {
        return axios
                .get(`${config.apiUrl}/stats/statsplayer/gameid=${gamdId}&&typeid=${typeId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static deleteStat = (statsId: string): Promise<any>  => {
        return axios
                .delete(`${config.apiUrl}/stats/${statsId}`)
                .then((result: AxiosResponse) => result.data);
    }

    public static insertStat = (stats: IStatus): Promise<IStatus> => {
        return axios
                .post(`${config.apiUrl}/stats`, stats)
                .then((result: AxiosResponse) => result.data);
    }

    public static updateStat = (statsId: string, stats: IStatus): Promise<IStatus> => {
        return axios
                .put(`${config.apiUrl}/stats/${statsId}`, stats)
                .then((result: AxiosResponse) => result.data)
    }
}