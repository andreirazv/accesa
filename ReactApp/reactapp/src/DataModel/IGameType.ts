export interface IGameType {
    _id?: string;
    typeName: string;
    allowTeam: boolean;
    remainingPoints: number;
    time: number;
    rounds: number;
    countPlayed: number;
    photoId?: string;
}