export interface IRequest {
    imageBase64: string;
    token: string;
}
