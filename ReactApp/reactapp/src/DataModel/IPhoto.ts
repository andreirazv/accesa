export interface IPhoto {
    _id?: string;
    sourcePhoto: string;
}