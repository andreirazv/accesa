export interface IResult {
    _id?: string;
    teamId: string;
    gameId: string;
    typeGameId: string;
    statsId: string;
    date?: Date;
}