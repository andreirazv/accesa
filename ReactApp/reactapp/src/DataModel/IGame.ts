export interface IGame {
    _id?: string;
    gameName: string;
    gameRules: string;
    countPlayed: string;
    gamePhotoId?: string;
}