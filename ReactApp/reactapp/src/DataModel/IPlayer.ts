export interface IPlayer {
    _id?: string;
    playerName: string;
    username?: string;
    password: string;
    totalStatsId: string;
    playerPhotoId: string;
    registerDate?: string;
    token?: string;
}