export interface ILeaderBoard {
    _id?: string;
    position: number;
    name: string;
    typeName?: string;
    wins: number;
    defeats: number;
    draws: number;
}