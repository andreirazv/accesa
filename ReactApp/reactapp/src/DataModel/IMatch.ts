export interface IMatch {
    _id?: string;
    gameId: string;
    typeGameId: string;
    teamsAndPoints?: {};
    winnerTeam: string;
    finalPhotoId: string;
    DateEndMatch?: string;
}