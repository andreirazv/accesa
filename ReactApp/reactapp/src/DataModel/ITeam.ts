export interface ITeam {
    _id?: string;
    teamName: string;
    players: string[];
    photoId?: string;
}