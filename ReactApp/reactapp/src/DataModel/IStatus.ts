export interface IStatus {
    _id?: string;
    wins: string;
    defeats: string;
    draws: string;
}