import * as React from 'react';
import './RankPanel.css';
import { ILeaderBoard } from 'src/DataModel/ILeaderBoard';
import LeaderBoardService from 'src/services/LeaderBoardService';
import LoadingAnim from '../Loading/loading';
import Demo from './TableReact';
export interface IRankState {
  players: ILeaderBoard[];
  loading: boolean;
}

export default class RankPanel extends React.Component<any, IRankState> {

  constructor(props:any) {
    super(props);
    this.state = {
      loading: true,
      players:[]
    }
  }
  public componentDidMount() {
    LeaderBoardService.getPlayersStatus().then((players: ILeaderBoard[]) => {
        this.setState({
            players: players,
            loading:false
        });
    });
}// <LeaderBoardList players={this.state.players}/>

  public render() {
    return (
      <div>
             {this.state.loading&&<LoadingAnim/>  }
                {!this.state.loading &&
                  <Demo></Demo>
                }
      </div>
    )
  }
}