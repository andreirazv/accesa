import * as React from 'react';
import {
  PagingState, CustomPaging,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  PagingPanel,
} from '@devexpress/dx-react-grid-bootstrap3';
import LoadingAnim from '../Loading/loading';
import LeaderBoardService from 'src/services/LeaderBoardService';
import { ILeaderBoard } from 'src/DataModel/ILeaderBoard';


const URL = 'http://localhost:64567/api/player/pagg';
export interface ITablePagState{
    columns:any[];
    rows:ILeaderBoard[];
    totalCount:number;
    pageSize: number;
    currentPage:number;
    loading: boolean;
    lastQuery: string;
}
export interface ITablePagProps{

}
export default class Demo extends React.Component<ITablePagProps,ITablePagState> {
  constructor(props:ITablePagProps) {
    super(props);

    this.state = {
      columns: [
        { name: 'position', title: 'position' },
        { name: 'name', title: 'name' },
        { name: 'wins', title: 'wins' },
        { name: 'defeats', title: 'defeats' },
        { name: 'draws', title: 'draws' },
      ],
      rows: [],
      totalCount: 0,
      pageSize: 2,
      currentPage: 0,
      loading: true,
      lastQuery: ''
    };

    this.changeCurrentPage = this.changeCurrentPage.bind(this);
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate() {
    const queryString = this.queryString();
    if (queryString != this.state.lastQuery) {
      this.loadData();
    }
  }

  changeCurrentPage(currentPage:number) {
    this.setState({
      loading: true,
      currentPage,
    });
  }

  queryString() {
    return `${URL}&take=${this.state.pageSize}&skip=${this.state.pageSize * this.state.currentPage}`;
  }

  loadData() {
    const queryString = this.queryString();
    if (queryString === this.state.lastQuery) {
      this.setState({ loading: false });
      return;
    }
    LeaderBoardService.getPaggData(queryString).then((leader:ILeaderBoard[])=>{
        LeaderBoardService.getPlayersStatus().then((len:ILeaderBoard[])=>{
        this.setState({
            rows: leader,
            totalCount: len.length,
            loading: false,
            lastQuery:queryString,
          });
        });
    }).catch(() => this.setState({ loading: false }));
  }

  render() {
    return (
      <div style={{ position: 'relative',backgroundColor:'rgba(0,0,0,0.4)',overflow:'hiddent' }}>
        <Grid
          rows={this.state.rows}
          columns={this.state.columns}
        >
          <PagingState
            currentPage={this.state.currentPage}
            onCurrentPageChange={this.changeCurrentPage}
            pageSize={this.state.pageSize}
          />
          <CustomPaging
            totalCount={this.state.totalCount}
          />
           <PagingPanel />
          <Table />
          <TableHeaderRow />

        </Grid>
        {this.state.loading && <LoadingAnim />}
      </div>
    );
  }
}
