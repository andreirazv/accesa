import * as React from 'react';
import './Photo.css';
import { Link } from 'react-router-dom';
import { IPhoto } from 'src/DataModel/IPhoto';


export interface IPhotoProps {
    item: IPhoto;
    selectPhoto(item: IPhoto): void;
    deletePhoto(item: IPhoto): void;
}

const Photo = (props: IPhotoProps) => {
    return (
        <div key={props.item._id} >
            <span className='photo' onClick={() => { props.selectPhoto(props.item) }}>{`${props.item.sourcePhoto}`}</span>
            <button><Link to={`/manage/photo/${props.item._id}`}>Update</Link></button>
            <button onClick={() => props.deletePhoto(props.item)}>Delete</button>
        </div>
    );
}

export default Photo;