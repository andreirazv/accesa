import * as React from 'react';
import { IPhoto } from 'src/DataModel/IPhoto';
import Photo from './Photo/Photo';
import LoadingAnim from 'src/components/Loading/loading';
import PhotosService from 'src/services/PhotosService';

export interface IPhotosListProps {
    onPhotoSelected(id: string): void;
}

export interface IPhotosState {
    photos: IPhoto[];
    loading: boolean;
}

export default class PhotosList extends React.Component<IPhotosListProps, IPhotosState> {
    constructor(props: IPhotosListProps) {
        super(props);
        this.state = {
            photos: [],
            loading: true
        }
    }

    public componentDidMount() {
        PhotosService.getAllPhotos().then((photos: IPhoto[]) => {
            this.setState({
                photos: photos,
                loading: false
            });
        });
    }

    public selectPhoto = (photo: IPhoto) => {
        if (!photo._id) return;
        this.props.onPhotoSelected(photo._id);
    }

    public deletePhoto = (photo: IPhoto) => {
        if (!photo._id) return;
        PhotosService.deletePhoto(photo._id)
            .then(() => {
                this.setState((previousState: IPhotosState) => ({
                    photos: [...previousState.photos.filter(s => s._id !== photo._id)]
                }));
            })
    }

    public render() {

        return (
            <div>
                <h1>Photos List</h1>
                {this.state.loading&&<LoadingAnim/>  }

                {!this.state.loading &&
                    this.state.photos.map((item, index) => (
                        <Photo
                            key={index}
                            item={item}
                            selectPhoto={this.selectPhoto}
                            deletePhoto={this.deletePhoto}
                        >
                        </Photo>
                    )
                    )}
            </div>
        );
    }
}