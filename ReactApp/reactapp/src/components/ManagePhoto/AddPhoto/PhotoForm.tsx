import * as React from 'react';

export interface IPhotoFormProps {
    handleChange(e: any): void;
    source: string;
    nameAction: string;
}

const PhotoForm = (props: IPhotoFormProps) => {
    return (
        <div>
                <form action="/">
                <h1>{props.nameAction} Manager</h1>
                <label htmlFor="photo">Photo--nu este gata</label>
                <input type="file" name="photo" onChange={props.handleChange} value={props.source} accept="image/*"/><br /><br />

            </form>
        </div>
    );
}

export default PhotoForm;