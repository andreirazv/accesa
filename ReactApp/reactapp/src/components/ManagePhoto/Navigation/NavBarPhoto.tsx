import * as React from 'react';
import './NavBarPhoto.css';
import { Link } from 'react-router-dom';
export interface INavBarPhotoState {
}
export interface INavBarPhotoProps{
    handleChange(e:any):void;
    selectActivePage?:boolean;
}

export default class NavBarPhoto extends React.Component<INavBarPhotoProps,INavBarPhotoState> {

  constructor(props: INavBarPhotoProps) {
    super(props);
    
    this.state = {
    }
  }
    public render()
    {
        let allPhotosLink;
        let addButtonLink;
        if(this.props.selectActivePage==false)
        {
            allPhotosLink=<Link className="active" to='/manage/photo'>All Photos</Link>
            addButtonLink=<Link onClick={this.props.handleChange} to='/manage/photo'>Add Photo</Link> 
        }
        else
        {
            allPhotosLink=<Link onClick={this.props.handleChange} to='/manage/photo'>All Photos</Link>
            addButtonLink=<Link className="active" to='/manage/photo'>Add Photo</Link> 
        }
        return (
            <div>
                <ul className="ul-nav">
                    <li className="li-nav">
                        {allPhotosLink}           
                    </li>
                    <li className="li-nav">
                        {addButtonLink}            
                    </li> 
                </ul>
              </div>
        )
    }
}


