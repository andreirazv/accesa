import * as React from 'react';
import { IPhoto } from 'src/DataModel/IPhoto';
import PhotoForm from './AddPhoto/PhotoForm';
import PhotosService from 'src/services/PhotosService';


export interface IAddPhotoState {
    id?: string;
    sourcePhoto: string;
}
export default class AddPhoto extends React.Component<any, IAddPhotoState> {

    constructor(props:any) {
        super(props);
        this.state = {
            sourcePhoto:''
        }
    }
    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IAddPhotoState) => (
            { 
                ...prevState,
                [name]: value
            }
        ));
    }
    addPhoto = () => {
        const newPhoto: IPhoto = {
            sourcePhoto: this.state.sourcePhoto,
        };
        PhotosService.insertPhoto(newPhoto)
            .then(() => {
            });
    }
    public render() {
        return (
            <div>
                <PhotoForm
                source={this.state.sourcePhoto}
                nameAction={"Add"}
                handleChange={this.handleChange}/>
                {
                    <button onClick={this.addPhoto}>Add Photo</button>
                }
                
            </div>
        );
    }
}