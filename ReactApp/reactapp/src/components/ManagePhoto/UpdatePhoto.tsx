import * as React from 'react';
import { Link } from 'react-router-dom';
import { IPhoto } from 'src/DataModel/IPhoto';
import PhotoForm from './AddPhoto/PhotoForm';
import PhotosService from 'src/services/PhotosService';


export interface IUpdatePhotoState {
    id?: string;
    sourcePhoto:string;
}
export default class UpdatePhoto extends React.Component<any, IUpdatePhotoState> {

    constructor(props:any) {
        super(props);
        this.state = {
            sourcePhoto: '',
        }
    }

    componentDidMount() {
        if(this.props.match.params.id !== undefined) {
            PhotosService.getPhotoById(this.props.match.params.id)
                .then((photo: IPhoto) => {
                    this.setState({
                        sourcePhoto: photo.sourcePhoto,
                    });
                });
        } 
    }

    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IUpdatePhotoState) => (
            { 
                ...prevState,
                [name]: value
            }
        ));
    }
    updatePhoto = () => {
        const updatePhoto: IPhoto = {
            sourcePhoto:this.state.sourcePhoto
        }
        PhotosService.updatePhoto(this.props.match.params.id, updatePhoto)
            .then(() => {
                this.props.history.push('/manage/photo');
            });
    }
    public render() {
        return (
            <div>
                <PhotoForm
                nameAction={"Update"}
                source={this.state.sourcePhoto}
                handleChange={this.handleChange}/>
                <button onClick={this.updatePhoto}>Update</button>
                <Link to='/manage/photo'><button>Anuleaza</button></Link>
            </div>
        );
    }
}