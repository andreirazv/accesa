import * as React from 'react';
import PhotosList from './PhotosList/PhotosList';
import AddPhoto from './AddPhoto';
import NavBarPhoto from './Navigation/NavBarPhoto';
export interface IPhotoManagerState {
    photoId?: string;
    selectedPage?: boolean;
}
export interface IPhotoManagerProps {
    isAllPhotos?: boolean;
}

export default class PhotoManager extends React.Component<IPhotoManagerProps, IPhotoManagerState> {

  constructor(props: IPhotoManagerProps) {
    super(props);
    this.state = {
        selectedPage:false
    }
  }
  public photoSelected = (photoId: string) => {
    this.setState({
      photoId: photoId
    });
  }
public handleChange=()=>
{
    this.setState({
        selectedPage: !this.state.selectedPage
      });
      
}
    public render()
    {
        let pageRender;
        if(this.state.selectedPage==false)
        {
            pageRender=<PhotosList onPhotoSelected={this.photoSelected} />
        }
        else
        {
            pageRender=<AddPhoto/>
        }
        return (
            <div>
                <NavBarPhoto handleChange={this.handleChange} selectActivePage={this.state.selectedPage}/>
                {pageRender}
                {!!this.state.photoId}
            </div>
        )
    }
}