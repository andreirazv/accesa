import * as React from 'react';
import { Link } from 'react-router-dom';
import './Navigation.css'
import { IPlayer } from 'src/DataModel/IPlayer';
import PlayerService from 'src/services/PlayerService';
export interface INavigationState {
    logoImage: string;
    user: IPlayer;
}
export default class Navigation extends React.Component<any, INavigationState> {

    constructor(props: any) {
        super(props);
        this.state = {
            logoImage: '',
            user: Object(null)

          }
    }
    componentWillMount()
    {
        PlayerService.getPlayerById(localStorage.getItem("logged_playerId")||'').then((player:IPlayer)=>{
            this.setState({
                logoImage : "/Resources/gamepad.png",
                user : player
             });   
        });
    }
    logout()
    {
        localStorage.clear();
        sessionStorage.clear();
    }
    public render() {
        return (
            <div>
                <nav className="bar-nav">
                    <ul>
                        <li>
                            <Link to='/select/game'>Play the Game</Link>              
                        </li>
                        <li>
                            <Link to='/img-category'>Image Category(AI)</Link>              
                        </li>
                        <li>
                            <Link to='/add/game'>Add Game</Link>              
                        </li>
                        <li>
                            <Link to='/manage/gametype'>Manage GameType</Link>              
                        </li>
                        <li>
                            <Link to='/manage/Match'>Manage Match</Link>              
                        </li>
   

                    </ul>
                </nav>
                <nav className="bar-nav-status">
                        <a className="logo-img">
                            <Link to='/'>Game&nbsp;<img className="image-style" src={this.state.logoImage}>
                            </img>&nbsp;Room</Link>
                        </a>
                    <ul className="status-menu">
                        <li className="logged-view">
                            <Link to='/profile'>Profile</Link>              
                        </li>
                        <li className="btn-logout">
                            <Link onClick={this.logout} to='/'>Logout</Link>            
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}