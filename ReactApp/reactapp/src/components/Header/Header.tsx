import * as React from 'react';
import Navigation from './Navigation/Navigation';
import NavBeforeLogin from '../BeforeLogin/Navigation/NavBeforeLogin';
import AuthService from '../../services/AuthService';

export interface IHeaderState {
    Auth: AuthService;
  }

  export default class Header extends React.Component<any, IHeaderState> {
    constructor(props: any) {
        super(props);
        this.state = { 
            Auth:new AuthService,
        }
      }
    public render() {
        if(this.state.Auth.loggedIn()==true)
        return (
            <header>
            <Navigation />
        </header>
        )
        else
        return (
                <header>
                <NavBeforeLogin />
            </header>
        )
      }

}