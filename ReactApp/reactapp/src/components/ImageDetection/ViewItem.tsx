import * as React from 'react';
import './HandleCategory.css'
import { IPhoto } from 'src/DataModel/IPhoto';
import { IRequest } from 'src/DataModel/IRequest';
import PhotosService from 'src/services/PhotosService';
export interface IViewItemProps {
    photo:IPhoto;
    changeLoadingState():void;
    index:number;
    lenght:number;
}
export interface IViewItemState {
    rez:any;
    category:string;
}
export default class ViewItem extends React.Component<IViewItemProps, IViewItemState> {

    constructor(props: IViewItemProps) {
        super(props);
        const query = new URLSearchParams(location.search);
        this.state = {
                rez:'',
                category:query.get('category')||'',
          }
    }
    getPrediction(){
        var ret='';
        if(this.props.photo!=undefined)
        {
          ret = this.props.photo.sourcePhoto.replace('data:image/jpeg;base64,','');
          
       const req:IRequest = {
          imageBase64:ret,
          token:(localStorage.getItem("id_token_google")||'')
          };
          PhotosService.recognize(req).then((rez:any)=>{
            if(this.props.index==this.props.lenght-1)
            {this.props.changeLoadingState();}
            this.setState({ rez: rez });
          });
         
      }
    }
    componentDidMount()
    {
        if(this.state.rez==''||this.state.rez==undefined)
        {
            
            this.getPrediction();
        }
    }
    public render() {
       // <h3>Category:{JSON.stringify(this.state.rez.payload[0].displayName).replace('"','').replace('"','')}</h3>
       // <h3>Score:{JSON.stringify(this.state.rez.payload[0].classification.score)}</h3>
        return (
            <span>
                {this.state.rez!=undefined&&JSON.stringify(this.state.rez.payload)!=undefined&&
                JSON.stringify(this.state.rez.payload[0].displayName).replace('"','').replace('"','')==this.state.category
                &&
                <li>
                <img src={this.props.photo.sourcePhoto}></img>
                   {JSON.stringify(this.state.rez.payload)!=undefined&&
                   <span>
                   <h5>Category:{JSON.stringify(this.state.rez.payload[0].displayName).replace('"','').replace('"','')}</h5>
                   <h5>Score:{JSON.stringify(this.state.rez.payload[0].classification.score).replace('"','').replace('"','')}</h5>
                   </span>
                   }
                </li>
              
                }
                
            </span>
        )
    }
}