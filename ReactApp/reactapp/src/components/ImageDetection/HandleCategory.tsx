import * as React from 'react';
import './HandleCategory.css'
import { Link } from 'react-router-dom';
import GoogleLogin from 'react-google-login';
export interface IHandleCategoryState {
    existToken:boolean;

}
export default class HandleCategory extends React.Component<any, IHandleCategoryState> {

    constructor(props: any) {
        super(props);
        this.state = {
            existToken:false
          }
    }
    isTokenExpired(exp:any) {
        try {
            if (exp > Date.now()) { 
                return true;
            }
            else
                return false;
        }
        catch (err) {
            return false;
        }
    }
    componentDidMount()
    {
      let exp = localStorage.getItem("id_token_google_exp");
      let token = localStorage.getItem("id_token_google");
      if(exp==undefined||token==undefined||this.isTokenExpired(exp)==false)
      {
        this.setState({
          existToken:false
        });
      }
      else
      {
        this.setState({
          existToken:true
        });
      }
    }
    responseGoogle(response:any)
    {
      localStorage.setItem('id_token_google',response.Zi.access_token||'');
      localStorage.setItem('id_token_google_exp',response.Zi.expires_at||'');
      history.go();
    }
    public render() {
        return (
            <div className="category-box">
                  {!this.state.existToken&&
                    <div>
                        <h2>Please login with google!</h2>
                        <GoogleLogin
                                    clientId="287859511961-ovcrvc0m41s7hpnj4h1mb6vtdudfcrcl.apps.googleusercontent.com"
                                    buttonText="Login"
                                    scope="https://www.googleapis.com/auth/cloud-platform https://www.googleapis.com/auth/cloud-vision"
                                    onSuccess={this.responseGoogle}
                                    onFailure={this.responseGoogle}/>
                    </div>
                    }
              {this.state.existToken&& <ul>
                   <li><Link to="/view-only?category=darts">Darts</Link></li>
                   <li><Link to="/view-only?category=fussball">Foosball</Link></li>
                   <li><Link to="/view-only?category=fifa19">Fifa</Link></li>
                   <li><Link to="/view-only?category=group">Group</Link></li>
                   <li><Link to="/view-only?category=people">People</Link></li>
               </ul>}
            </div>
        )
    }
}