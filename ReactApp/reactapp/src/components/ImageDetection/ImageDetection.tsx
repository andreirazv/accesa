import * as React from 'react';
import './ImageDetection.css';
import FileBase64 from './imageToBase64.js';
import { IRequest } from 'src/DataModel/IRequest';
import PhotosService from 'src/services/PhotosService';
import GoogleLogin from 'react-google-login';
export interface IImageState
{
  files: any;
  rez: any;
  existToken: boolean;
}
export default class ImageDetection extends React.Component<any,IImageState> {

  constructor(props:any) {
    super(props)
    this.state = {
      files: '',
      rez: '',
      existToken:false
    }
  }
  isTokenExpired(exp:any) {
    try {
        if (exp > Date.now()) { 
            return true;
        }
        else
            return false;
    }
    catch (err) {
        return false;
    }
}
componentDidMount()
{
  let exp = localStorage.getItem("id_token_google_exp");
  let token = localStorage.getItem("id_token_google");
  if(exp==undefined||token==undefined||this.isTokenExpired(exp)==false)
  {
    this.setState({
      existToken:false
    });
  }
  else
  {
    this.setState({
      existToken:true
    });
  }
}
  getFiles(files:any){
      this.setState({ files: files })
  }
   getPrediction(){
    var ret='';
    if(this.state.files!='')
    {
      ret = this.state.files.base64.replace('data:image/jpeg;base64,','');
      
   const req:IRequest = {
      imageBase64:ret,
      token:(localStorage.getItem("id_token_google")||'')
      };
      PhotosService.recognize(req).then((rez:any)=>{
        this.setState({ rez: rez });
      });
  }
}
responseGoogle(response:any)
{
  localStorage.setItem('id_token_google',response.Zi.access_token||'');
  localStorage.setItem('id_token_google_exp',response.Zi.expires_at||'');
  history.go();
}
  render() {
    return (
      <div>
        {this.state.existToken&&
      <div className="image-detection">
      
        <div>
        <h1>Image Recognition</h1>
        <div>
          <p>Upload photo [darts, fifa19,foosball]</p>
          <FileBase64
            multiple={ false }
            onDone={ this.getFiles.bind(this) } />
        </div>

        <div>
          {this.getPrediction()}
          {this.state.rez!=''&&
            <div> <img src={this.state.files.base64}/>
            <h3>Game:{JSON.stringify(this.state.rez.payload[0].displayName).replace('"','').replace('"','')}</h3>
            <h3>Score:{JSON.stringify(this.state.rez.payload[0].classification.score)}</h3>
             </div>
          }
        </div>
        </div>
      
      </div>}
      {!this.state.existToken&&
      <div>
        <h2>Please login with google!</h2>
        <GoogleLogin
                    clientId="287859511961-ovcrvc0m41s7hpnj4h1mb6vtdudfcrcl.apps.googleusercontent.com"
                    buttonText="Login"
                    scope="https://www.googleapis.com/auth/cloud-platform https://www.googleapis.com/auth/cloud-vision"
                    onSuccess={this.responseGoogle}
                    onFailure={this.responseGoogle}/>
      </div>
      }
      </div>
    );

  }

}