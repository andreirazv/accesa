import * as React from 'react';
import './HandleCategory.css'
import { IPhoto } from 'src/DataModel/IPhoto';
import PhotosService from 'src/services/PhotosService';
import ViewItem from './ViewItem';
import LoadingAnim from '../Loading/loading';
export interface IViewCategoryState {
    allPhoto:IPhoto[];
    category:string;
    loading:boolean;
}
export default class ViewCategory extends React.Component<any, IViewCategoryState> {

    constructor(props: any) {
        const query = new URLSearchParams(location.search);
        super(props);
        this.state = {
            allPhoto:[],
            category:query.get('category')||'',
            loading:true,
          }
          this.changeLoadingState=this.changeLoadingState.bind(this);
    }
    componentDidMount()
    {
        PhotosService.getAllPhotos().then((photos:IPhoto[])=>{
            this.setState({
                allPhoto : photos
             });   
        });
    }
    changeLoadingState(){
        this.setState({loading:!this.state.loading});
    }
    public render() {
        console.log("render view category");
        return (
            <div>
                {this.state.loading&&<LoadingAnim/>}
            <div className="category-box-item">
            
            {this.state.category!=undefined&&this.state.allPhoto!=undefined&&
               <ul>{this.state.allPhoto.map((item:IPhoto,index:number)=>{
                    return (<ViewItem lenght={this.state.allPhoto.length} index={index} changeLoadingState={this.changeLoadingState} key={index} photo={item}/>)
                })
                }
               </ul>
            }
            </div>
            </div>
        )
    }
}