import * as React from 'react';
import { IPhoto } from 'src/DataModel/IPhoto';
import { ITeam } from 'src/DataModel/ITeam';
import MatchService from 'src/services/MatchService';
import { IMatch } from 'src/DataModel/IMatch';
import TeamService from 'src/services/TeamService';
import PhotosService from 'src/services/PhotosService';

export interface IFinishedBattleState {
	match:string;
	isFinishedMatch:boolean;
	finalPhoto:IPhoto;
	winnerTeam:ITeam;
  }
export default class FinishedBattle extends React.Component<any,IFinishedBattleState > {

  constructor(props: any) {
		super(props);
		const query = new URLSearchParams(this.props.location.search);
    this.state = {
		match:query.get('match')||'',
		winnerTeam:Object(null),
		finalPhoto:Object(null),
		isFinishedMatch:query.get('finished')==='true',
    }
  }

  componentWillMount()
{
    if(!this.state.isFinishedMatch)
    {
        history.pushState('','','/leaderboard');
        history.go();
    }
    else
    {
        if(this.state.match!=undefined)
        {
            MatchService.getMatchById(this.state.match).then((match:IMatch)=>{
                if(match.winnerTeam!=undefined&&match.finalPhotoId!=undefined)
                TeamService.getTeamById(match.winnerTeam).then((team:ITeam)=>{
                    PhotosService.getPhotoById(match.finalPhotoId).then((photo:IPhoto)=>{
                        this.setState({
                            winnerTeam:team,
                            finalPhoto:photo
                        });
                    });
                });


            });
        }
    }
}
  public render() {
    return (
      <div className="bg-finished">
		  {this.state.isFinishedMatch&&this.state.winnerTeam.teamName!=undefined&&this.state.finalPhoto.sourcePhoto!=undefined&&
		  <div>
			<h1>This game was finished!</h1>
            {this.state.winnerTeam.players.length==1&&
			    <h3></h3>
            }
            {this.state.winnerTeam.players.length!=1&&
			    <h3>Name of the Winner Team:{this.state.winnerTeam}</h3>
            }
			<h3>Picture:</h3>
				<img src={this.state.finalPhoto.sourcePhoto}/>
			</div>
          }
      </div>
    )
  }
}
