import * as React from 'react';
import { IPlayer } from 'src/DataModel/IPlayer';
import { IStatus } from 'src/DataModel/IStatus';
import StatsService from 'src/services/StatsService';
import { IGame } from 'src/DataModel/IGame';
import { IGameType } from 'src/DataModel/IGameType';
import PlayerService from 'src/services/PlayerService';
import './BattleItem.css';
import PhotosService from 'src/services/PhotosService';
import { IPhoto } from 'src/DataModel/IPhoto';
export interface IBattleItemProps{
    playerRecv:string
    pointsForEachPlayer:boolean
}
export interface IBattleItemStat{
	statsPlayer:IStatus;
	statsPlayerForGameAndType:IStatus;
	game:IGame;
	type:IGameType;
	player:IPlayer;
	base64Img:string;
	points:number;
}
export default class BattleItem extends React.Component<IBattleItemProps,IBattleItemStat > {

  constructor(props: IBattleItemProps) {
    super(props);

    this.state = {
        statsPlayer:Object(null),
        statsPlayerForGameAndType:Object(null),
        game:Object(null),
        type:Object(null),
        player:Object(null),
        base64Img:'',
        points:0
    }
  }
  componentWillMount()
  {
    PlayerService.getPlayerById(this.props.playerRecv).then((player:IPlayer)=>{
        StatsService.getStatById(player.totalStatsId).then((statistics:IStatus)=>{
            PhotosService.getPhotoById(player.playerPhotoId).then((photo:IPhoto)=>{
                this.setState({
                    statsPlayer:statistics,
                    player:player,
                    base64Img:photo.sourcePhoto
                });
            });
        });
    })
    const query = new URLSearchParams(location.search);
    const gameId=query.get('game')||'';
    const typeId=query.get('type')||'';
    if(gameId!=undefined&&typeId!=undefined)
    { 
        StatsService.getStatsForPlayer(gameId,typeId).then((statistics:IStatus)=>{
            this.setState({
                statsPlayerForGameAndType:statistics
            });
        });
    }
  }
  public render() {
    return (
        <span className="player-box" key={this.state.player._id}>
            <img className="player-img" src={this.state.base64Img} alt={`${this.state.player.playerName}`} />
            <h3>Nume: {this.state.player.playerName}</h3>
            {this.props.pointsForEachPlayer&&<section>
                <h4>Points: {this.state.points}</h4>
                <img className="plus-img" src="/Resources/plus.png"/>
            </section>
            }
            <img className="info-img" src="/Resources/info.png"/>
            <article className="info-player">
                <h4>Score for this game and type:</h4>
                <h5>Wins: {this.state.statsPlayerForGameAndType.wins}</h5>
                <h5>Defeats: {this.state.statsPlayerForGameAndType.defeats}</h5>
                <h5>Draws: {this.state.statsPlayerForGameAndType.draws}</h5>
            </article>
        </span>
    )
  }
}