import * as React from 'react';
import TeamBattleItem from './TeamItemBattle';
import MatchService from 'src/services/MatchService';
import { IMatch } from 'src/DataModel/IMatch';
import ModalForm from 'src/components/ModalForm/ModalForm';
import EndGame from '../EndGameModal/EndGame';
import TeamService from 'src/services/TeamService';
import { ITeam } from 'src/DataModel/ITeam';
export interface IHandlerBattleState {
	gameId: string;
	typeId: string;
	teamList:String[];
	forEachPlayer:boolean;
	match:IMatch;
	isFinishedMatch:boolean;
	showModal: boolean;
	winnerTeamId: string;

  }
export default class HandlerBattle extends React.Component<any,IHandlerBattleState > {

  constructor(props: any) {
		super(props);
		const query = new URLSearchParams(this.props.location.search);
		let teamString:string = query.get('teams')||'';
    this.state = {
		gameId:(query.get('game')||''),
		typeId:(query.get('type')||''),
		teamList:(teamString.split(',')),
		forEachPlayer:(query.get('foreach')||'')==='true',
		match:Object(null),
		isFinishedMatch:false,
		showModal:false,
		winnerTeamId:''
		}
		this.changeModalState = this.changeModalState.bind(this);
  }

  componentWillMount()
{
	const query = new URLSearchParams(this.props.location.search);
	let matchId = query.get('match');


	if(matchId!=undefined)
	{
		MatchService.getMatchById(matchId||'').then((checkMatch:IMatch)=>{

			if(checkMatch!=undefined&&checkMatch._id!=undefined)
			{

				if(checkMatch.finalPhotoId==''||checkMatch.winnerTeam=='')
				{

					this.setState({
						isFinishedMatch:false,
						match:checkMatch
					});

				}
				else
				{
					this.setState({
						isFinishedMatch:true,
						match:checkMatch
					});
				}
			}

		});
	}
	else
	{
		this.createNewMatch(query);
	}
}
isFinishedGame()
{
	console.log("asdsad::"+this.state.isFinishedMatch);
	if(this.state.isFinishedMatch)
	{
		history.pushState('','',"play/finished?match="+this.state.match._id+"&finished="+this.state.isFinishedMatch);
		//history.go();
	}
}
createNewMatch(query:any)
{
	if(!this.state.isFinishedMatch)
		{
			let listTeam={};
			let playerList={};
			if(!this.state.forEachPlayer)
			{
				this.state.teamList.map((item:string,index:any)=>{
					console.log("team:"+item);
					listTeam[item]=0;
				});
			}
			else
			{
				this.state.teamList.map((item:string,index:any)=>{
					TeamService.getTeamById(item).then((team:ITeam)=>{
						team.players.map((item:string,index:any)=>{
							playerList[item]=0;
						});
					})
					listTeam[item]=playerList;
				});
			}
			const match:IMatch={
			gameId:this.state.gameId,
			typeGameId:this.state.typeId,
			teamsAndPoints:listTeam,
			winnerTeam:'',
			finalPhotoId:''
		};
		MatchService.insertMatch(match).then((matchGet:IMatch)=>{
		if(query.get('match'))
		{
			query.set('match',matchGet._id||'');
			history.replaceState('','',location.pathname+"?game="+query.get('game')
			+"&type="+query.get('type')+"&foreach="+query.get('foreach')+"&teams="+query.get('teams')+"&match="+query.get('match'));
		}	
		else
			history.replaceState('','',this.props.location.search+"&match="+matchGet._id);
		this.setState({
			match:matchGet
		});
		}).catch((err:Error)=>{
			alert(err.message);
		});
	}
}
public finisGame=()=>
{	
	console.log("finishGame");
	if(!this.state.isFinishedMatch)
	{
		this.setState({
			isFinishedMatch:true
		});
	}
}

changeModalState() {
	this.isFinishedGame();
		this.setState({
			showModal: !this.state.showModal,
			});
}

  public render() {
	this.isFinishedGame();
    return (
      <div>
		 {this.state.match._id!=undefined&&!this.state.isFinishedMatch&&
		  	<div>
			  <button className="btn-start" onClick={this.changeModalState}>EndGame</button>	

				{this.state.teamList.map((item:string,index:number) => {	
					return <TeamBattleItem match={this.state.match} pointsForEachPlayer={this.state.forEachPlayer} key={index} teamStr={item}/>						
				})}
			</div>}
		
			{this.state.showModal &&<ModalForm component={EndGame} showModal = {this.changeModalState}/>}

      </div>
    )
  }
}
