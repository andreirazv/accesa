import * as React from 'react';
import BattleItem from './BattleItem';
import './TeamBattleItem.css';
import { ITeam } from 'src/DataModel/ITeam';
import { IMatch } from 'src/DataModel/IMatch';
import MatchService from 'src/services/MatchService';
import TeamService from 'src/services/TeamService';

export interface ITeamBattleItemProps{
  teamStr:string;
  pointsForEachPlayer:boolean;
  match:IMatch;
}
export interface ITeamBattleState{
  points:number;
  match:IMatch;
  team:ITeam;
  forEach:boolean;
}
export default class TeamBattleItem extends React.Component<ITeamBattleItemProps,ITeamBattleState > {

  constructor(props: ITeamBattleItemProps) {
    super(props);
    const query = new URLSearchParams(location.search);
    this.state = {
      points:0,
      match:this.props.match,
      team:Object(null),
      forEach:query.get('foreach')==='true'
    }
  }
  getPoints = (idMatch:string,idTeam:string)=> {
    if(!this.state.forEach)
   {
      MatchService.getTeamPoints(idMatch,idTeam).then((pcts:number)=>{
      console.log("puncte1:"+pcts);
      this.setState({
        points:pcts
      });
      });
    }
  }
  componentDidMount()
  {
    TeamService.getTeamById(this.props.teamStr).then((tm:ITeam)=>{
      this.getPoints(this.state.match._id||'',tm._id||'');
      this.setState({
        team:tm,
      });
      
  });  
  }

  addPoints=()=>
  {
    MatchService.addPoints(this.props.match._id||'',this.state.team._id||'','1').then((match:IMatch)=>{
      this.setState({
        match:match
      });
      this.getPoints(this.state.match._id||'',this.state.team._id||'');
  });
}
  public render() {
    console.log("render");
    return (
      <div className="team-box">
        {this.state.team.players!=undefined&&<span>
            <header>
              {this.state.team.players.length>1 &&<h2>{this.state.team.teamName}</h2>}
                <section>
                    {this.props.pointsForEachPlayer&&<span className="average"><h5>Average Points:0</h5></span>}
                    <span className="points"><h4>Points: {this.state.points}</h4></span>
                    {!this.props.pointsForEachPlayer&& <button className="plus" onClick={()=>this.addPoints()}><img src="/Resources/plus.png"/></button>}
                </section>
            </header>
        {
              this.state.team.players.map((item, index) => (
                <BattleItem pointsForEachPlayer={this.props.pointsForEachPlayer} key={index} playerRecv={item}></BattleItem>
            ))
        }
      </span> }
       </div>
    )
  }
}