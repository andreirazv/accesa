import * as React from 'react';
import GameList from './GameManager/GameList';
import './GamePlay.css';
export interface IGamePlayState {
  gameId?: string;
  typeId?: string;
  teamId?: string;
}

export default class GamePlay extends React.Component<any, IGamePlayState> {

  constructor(props: any) {
    super(props);

    this.state = {
    }
  }

  public render() {
    return (
      <div>
        <GameList/>
      </div>
    )
  }
}