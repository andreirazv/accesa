import * as React from 'react';
import GameTypesList from 'src/components/ManageGameType/GameTypesList/GameTypesList';
import SelectType from './SelectType';
import '../GameManager/SelectGame.css';
export interface ISelectGameState {
  gameTypeId?: string;
  gameId?: string;
}

export default class GameTypeList extends React.Component<any, ISelectGameState> {

  constructor(props: any) {
    super(props);
    const query = new URLSearchParams(this.props.location.search);
    this.state = {
      gameId:(query.get('game')||'')
    }
  }

  public gameTypeSelected = (gameTypeId: string) => {
    this.setState({
      gameTypeId: gameTypeId
    });
  }

  public render() {
    return (
      <div>
        <h2><button onClick={()=>history.back()} className="btn-back">Back</button></h2>
        <GameTypesList onGameTypeSelected={this.gameTypeSelected} selectType={SelectType} />
      </div>
    )
  }
}