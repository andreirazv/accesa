import * as React from 'react';
import '../GameManager/SelectGame.css';
import { IGameType } from 'src/DataModel/IGameType';
import PhotosService from 'src/services/PhotosService';
import { IPhoto } from 'src/DataModel/IPhoto';
declare module '*.png'

export interface ISelectTypeProps {
    item: IGameType;
    selectGameType(item: IGameType): void;
    handleChnage(): void;
}

export interface ISelectTypeState {
    game:string;
    base64Img:string;
}


export default class SelectType extends React.Component<ISelectTypeProps, ISelectTypeState> {
    constructor(props: ISelectTypeProps) {
        super(props);
        const query = new URLSearchParams(location.search);
        this.state = {
            game:(query.get('game')||''),
            base64Img:''
          }
    }
    componentDidMount()
    {
        PhotosService.getPhotoById(this.props.item.photoId||'').then((image:IPhoto)=>{
            this.setState({
                base64Img:image.sourcePhoto
            });
        });
    }
    SelectType=(gameType:IGameType)=>
    {
        if(gameType._id!=undefined) {
            history.pushState('','',"team?game="+this.state.game+"&type="+gameType._id);
            history.go();
        }
        else
        {
            alert("There is a problem with selected gameType!");
        }
    }
   
    render(){
        return (

            <span key={this.props.item._id} >
            <button onClick={() => {this.SelectType(this.props.item) }} className="select-wrap">
                    <img src={this.state.base64Img} className="select-img" alt={`${this.props.item.typeName}`} />
                    <article className="select-description">
                        <h2>{`${this.props.item.typeName}`}</h2>
                        <p>{`Allow Team: ${this.props.item.allowTeam}`}</p>
                        <p>{`Point to achive: ${this.props.item.remainingPoints}`}</p>
                        <p>{`Rounds: ${this.props.item.rounds}`}</p>
                        <p>{`Time: ${this.props.item.time}`}</p>
                        <p>{`Count Played: ${this.props.item.countPlayed}`}</p>
                    </article>
                </button></span>
        );
    }
}

