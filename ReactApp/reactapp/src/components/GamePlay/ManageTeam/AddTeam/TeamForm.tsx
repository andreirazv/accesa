import * as React from 'react';

export interface ITeamFormProps {
    handleChange(e: any): void;
    teamName: string;
    players: string[];
    nameAction: string;
}

const TeamForm = (props: ITeamFormProps) => {
    return (
        <div>
                <form action="/">
                <h1>{props.nameAction} Manager</h1>
                <label htmlFor="teamname">Team Name</label>
                <input type="text" name="teamname" onChange={props.handleChange} value={props.teamName}/><br /><br />                       
            </form>
        </div>
    );
}

export default TeamForm;