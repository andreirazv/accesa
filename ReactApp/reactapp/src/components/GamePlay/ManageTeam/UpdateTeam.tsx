import * as React from 'react';
import TeamForm from './AddTeam/TeamForm';
import { ITeam } from 'src/DataModel/ITeam';
import TeamsService from 'src/services/TeamService';
import { Link } from 'react-router-dom';


export interface IUpdateTeamState {
    id?: string;
    teamName: string;
    players: string[];
}
export default class UpdateTeam extends React.Component<any, IUpdateTeamState> {

    constructor(props:any) {
        super(props);
        this.state = {
            teamName: '',
            players: []
        }
    }

    componentDidMount() {
        if(this.props.match.params.id !== undefined) {
            TeamsService.getTeamById(this.props.match.params.id)
                .then((team: ITeam) => {
                    this.setState({
                        teamName: team.teamName,
                        players: team.players,
                    });
                });
        } 
    }

    handleChange = (e: any) => {
        const { teamName, value } = e.target;
        this.setState((prevState: IUpdateTeamState) => (
            { 
                ...prevState,
                [teamName]: value
            }
        ));
    }
    updateTeam = () => {
        const updateTeam: ITeam = {
            teamName:this.state.teamName,
            players:this.state.players
        }
        TeamsService.updateTeam(this.props.match.params.id, updateTeam)
            .then(() => {
                this.props.history.push('/manage/team');
            });
    }
    public render() {
        return (
            <div>
                <TeamForm
                nameAction={"Update"}
                teamName={this.state.teamName}
                players={this.state.players}
                handleChange={this.handleChange}/>
                <button onClick={this.updateTeam}>Update Team</button>
                <Link to='/manage/team'><button>Anuleaza</button></Link>
            </div>
        );
    }
}