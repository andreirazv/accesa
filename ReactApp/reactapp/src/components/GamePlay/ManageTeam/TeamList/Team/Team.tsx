import * as React from 'react';
import 'src/components/ManagePlayers/PlayersList/Player/Player.css';
import { ITeam } from 'src/DataModel/ITeam';
import PlayerService from 'src/services/PlayerService';
import { IPlayer } from 'src/DataModel/IPlayer';
import { IPhoto } from 'src/DataModel/IPhoto';
import PhotosService from 'src/services/PhotosService';

export interface ITeamProps {
    item: ITeam;
    selectTeam(item: ITeam): void;
}
export interface ITeamState{
    namePlayers:string[];
    base64Img:string;
}
export default class Team extends React.Component<ITeamProps, ITeamState> {
    constructor(props: ITeamProps) {
        super(props);
        this.state = {
            namePlayers:[],
            base64Img:''
          }
    }
    componentWillMount()
    {
        let prevList:string[]=[];
        
        this.props.item.players.map((item, index) => (
            PlayerService.getPlayerById(item).then((player:IPlayer)=>{
                prevList.push(player.playerName);
                PhotosService.getPhotoById(this.props.item.photoId||'').then((photo:IPhoto)=>{
                this.setState({
                    namePlayers:prevList,
                    base64Img:photo.sourcePhoto
                });
            });
        })));
    }
    render(){ 
        return ( 
            <span key={this.props.item._id}>
            {this.state.namePlayers.length>1&&
            <button className="player-wrap" onClick={() => {this.props.selectTeam(this.props.item) }} > 
            <input className="native-hidden" id={`ckbox${this.props.item._id}`} type="checkbox" />
                <label htmlFor={`ckbox${this.props.item._id}`}>
                    <img className="player-img" src={this.state.base64Img} alt={`${this.props.item.teamName}`} />
                    <article className="player-description">
                        <h2>{`${this.props.item.teamName}`}</h2>
                        <p>Players:</p>
                        { this.state.namePlayers.map((item, index) => (
                            <p key={index}>{`${item}`}</p>
                            ))}
                    </article> </label>
                    </button>
            }
        </span>
        );
    }
}

