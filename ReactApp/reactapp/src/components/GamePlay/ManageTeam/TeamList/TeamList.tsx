import * as React from 'react';
import { ITeam } from 'src/DataModel/ITeam';
import TeamsService from 'src/services/TeamService';
import LoadingAnim from 'src/components/Loading/loading';

export interface ITeamsListProps {
    onTeamSelected(team:ITeam): void;
    sTeam: any;
}

export interface ITeamsState {
    teams: ITeam[];
    loading: boolean;
}

export default class TeamsList extends React.Component<ITeamsListProps, ITeamsState> {
    constructor(props: ITeamsListProps) {
        super(props);
        this.state = {
            teams: [],
            loading: true
        }
    }

    public componentDidMount() {
        TeamsService.getAllTeams().then((teams: ITeam[]) => {
            this.setState({
                teams: teams,
                loading: false
            });
        });
    }

    public selectTeam = (team: ITeam) => {
        if (!team._id) return;
        this.props.onTeamSelected(team);
    }

    public deleteTeam = (team: ITeam) => {
        if (!team._id) return;
        TeamsService.deleteTeam(team._id)
            .then(() => {
                this.setState((previousState: ITeamsState) => ({
                    teams: [...previousState.teams.filter(s => s._id !== team._id)]
                }));
            })
    }

    public render() {

        return (
            <div>
                <h1>Teams List</h1>
                {this.state.loading&&<LoadingAnim/>  }

                {!this.state.loading &&
                    this.state.teams.map((item, index) => (
                        <this.props.sTeam
                            key={index}
                            item={item}
                            selectTeam={this.selectTeam}
                            deleteTeam={this.deleteTeam}
                        >
                        </this.props.sTeam>
                    )
                    )}
            </div>
        );
    }
}