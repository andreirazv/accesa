import * as React from 'react';
import './NavBarTeam.css';
export interface INavBarTeamState {
    
}
export interface INavBarTeamProps{
    handleChange(e:any):void;
    selectActivePage?:boolean;
}

export default class NavBarTeam extends React.Component<INavBarTeamProps,INavBarTeamState> {

  constructor(props: INavBarTeamProps) {
    super(props);
    
    this.state = {
    }
  }
  
    public render()
    {

        let singlePlayerlink;
        let teamPlayerLink;

        if(this.props.selectActivePage==false)
       {
            singlePlayerlink=<button className="active" onClick={this.props.handleChange}>Players</button>
            teamPlayerLink=<button onClick={this.props.handleChange}>Teams</button>
        }
       else
       {
            singlePlayerlink=<button onClick={this.props.handleChange}>Players</button>
            teamPlayerLink=<button className="active" onClick={this.props.handleChange}>Teams</button>
        }
        return (
            
            <div>
                
                <ul className="ul-nav">
                    <li className="li-nav">
                        {singlePlayerlink}           
                    </li>
                    <li className="li-nav">
                        {teamPlayerLink}            
                    </li> 
                </ul>
              </div>
        )
    }
}


