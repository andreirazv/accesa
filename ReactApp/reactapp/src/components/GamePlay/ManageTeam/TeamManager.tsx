import * as React from 'react';
import TeamsList from './TeamList/TeamList';
import Team from './TeamList/Team/Team';
import AddTeam from './AddTeam';
import NavBarTeam from './Navigation/NavBarTeam';
import { ITeam } from 'src/DataModel/ITeam';

export interface ITeamManagerState {
    teamId?: string;
    selectedPage?: boolean;
}
export interface ITeamManagerProps {
    isAllTeams?: boolean;
}

export default class TeamManager extends React.Component<ITeamManagerProps, ITeamManagerState> {

  constructor(props: ITeamManagerProps) {
    super(props);
    this.state = {
        selectedPage:false
    }
  }
  public teamSelected = (team: ITeam) => {
    this.setState({
      teamId: team._id
    });
  }
public handleChange=()=>
{
    this.setState({
        selectedPage: !this.state.selectedPage
      });
      
}
    public render()
    {
        return (
            <div>
                <NavBarTeam handleChange={this.handleChange} selectActivePage={this.state.selectedPage}/>
                {
                    !this.state.selectedPage&&<TeamsList onTeamSelected={this.teamSelected} sTeam={Team}/>
                }
                {
                    this.state.selectedPage&&<AddTeam/>
                }
                {!!this.state.teamId}
            </div>
        )
    }
}