import * as React from 'react';
import { ITeam } from 'src/DataModel/ITeam';
import TeamsService from 'src/services/TeamService';
import AddTeamForm from './AddTeam/TeamForm';


export interface IAddTeamState {
    id?: string;
    teamName: string;
    players: string[];

}
export default class AddTeam extends React.Component<any, IAddTeamState> {

    constructor(props:any) {
        super(props);
        this.state = {
            teamName: '',
            players: [],

        }
    }
    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IAddTeamState) => (
            { 
                ...prevState,
                [name]: value
            }
        ));
    }
    addTeam = () => {
        const newTeam: ITeam = {
            teamName: this.state.teamName,
            players: this.state.players
        };
        TeamsService.insertTeam(newTeam)
            .then(() => {
            });
    }
    public render() {
        return (
            <div>
                <AddTeamForm
                teamName={this.state.teamName}
                nameAction={"Add"}
                players={this.state.players}
                handleChange={this.handleChange}/>
                {
                    <button onClick={this.addTeam}>Add Team</button>
                }
                
            </div>
        );
    }
}