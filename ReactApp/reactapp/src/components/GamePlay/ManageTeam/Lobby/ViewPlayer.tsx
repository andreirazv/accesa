import * as React from 'react';
import './LobbyItem.css';
import PlayerService from 'src/services/PlayerService';
import { IPlayer } from 'src/DataModel/IPlayer';

export interface IViewProps {
    idPlayer: string;
}
export interface IViewState {
    player: IPlayer;
}
export default class ViewPlayer extends React.Component<IViewProps,IViewState> {

    constructor(props: IViewProps) {
      super(props);
      
      this.state = {
          player:Object(null)
      }
    }
    componentWillMount()
    {
        PlayerService.getPlayerById(this.props.idPlayer).then((player:IPlayer)=>{
            this.setState({
                player:player
            })
        });
    }
      public render()
      {
       return( 
        <span key={this.state.player._id}>
            <h3>{`${this.state.player.playerName}`}</h3>
        </span>
        
       );
      }
}
