import * as React from 'react';
import './LobbyItem.css';
import { IPlayer } from 'src/DataModel/IPlayer';

export interface ILobbyItemProps {
    item: IPlayer;
    selectItem(item: IPlayer): void;
    deleteItem(item: IPlayer): void;
}

const LobbyItemPlayer = (props: ILobbyItemProps) => {
    return (
        <div key={props.item._id} >
            <span className='team' onClick={() => { props.selectItem(props.item) }}>{`${props.item.playerName}`}</span>
            <button onClick={() => props.deleteItem(props.item)}>Delete</button>
        </div>
    )
}

export default LobbyItemPlayer;