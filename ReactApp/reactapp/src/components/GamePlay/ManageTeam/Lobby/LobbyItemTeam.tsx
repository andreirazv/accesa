import * as React from 'react';
import './LobbyItem.css';
import { ITeam } from 'src/DataModel/ITeam';

export interface ILobbyItemProps {
    item: ITeam;
    selectItem(item: ITeam): void;
    deleteItem(item: ITeam): void;
}

const LobbyItemTeam = (props: ILobbyItemProps) => {
    
    return (
        <div key={props.item._id} >
            <span className='team' onClick={() => { props.selectItem(props.item) }}>{`${props.item.teamName}`}</span>
            <button onClick={() => props.deleteItem(props.item)}>Delete</button>
        </div>
    );
    
}

export default LobbyItemTeam;