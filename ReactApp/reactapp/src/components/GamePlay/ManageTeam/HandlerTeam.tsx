import * as React from 'react';
import GameTypeService from 'src/services/GameTypeService';
import { IGameType } from 'src/DataModel/IGameType';
import NavBarTeam from './Navigation/NavBarTeam';
import PlayersList from 'src/components/ManagePlayers/PlayersList/PlayersList';
import Player from 'src/components/ManagePlayers/PlayersList/Player/Player';
import AuthService from '../../../services/AuthService';
import TeamsList from './TeamList/TeamList';
import Team from './TeamList/Team/Team';
import { IPlayer } from 'src/DataModel/IPlayer';
import { ITeam } from 'src/DataModel/ITeam';
import TeamService from 'src/services/TeamService';
import './HandlerTeam.css';
export interface IHandlerTeamState {
    Auth:AuthService;
    gameType?:IGameType;
    selectedPage:boolean;
    lobbyPlayer:any;
    lobbyTeam:any;
    selectedItemsToPlay:any;
    teamList:String[];
    playerList:IPlayer[];
    stepInput:any;
    gameId:string;
}
export default class AddTeam extends React.Component<any, IHandlerTeamState> {

    constructor(props:any) {
        super(props);
        const query = new URLSearchParams(this.props.location.search);
        this.state = {
            Auth:new AuthService,
            selectedPage:false,
            lobbyPlayer:<PlayersList onPlayerSelected={this.addPlayerOrDelete} sPlayer={Player}/>,
            lobbyTeam:<TeamsList onTeamSelected={this.addTeamOrDelete} sTeam={Team}/>,
            selectedItemsToPlay:null,
            teamList:[],
            playerList:[],
            stepInput:false,
            gameId:(query.get('game')||'')
        }
        
    }
    componentWillMount()
    {
        const query = new URLSearchParams(this.props.location.search);
        GameTypeService.getGameTypeById(query.get('type')||'').then((gameTypes: IGameType) => {
            this.setState({
                gameType: gameTypes,
            });
        });
        this.addLoggedPlayer();
    }
    public addLoggedPlayer()
    {
        this.state.Auth.getPlayerLogged().then((player: IPlayer) => {
            this.addPlayerOrDelete(player);  
        });  
    }
    public handleChange=()=>
    {
        this.setState({
            playerList:[],
            teamList:[]
        });
        if(this.state.selectedPage)
        this.addLoggedPlayer();

        this.setState({
            selectedPage: !this.state.selectedPage
        });
    }

    public addPlayerOrDelete = (player: IPlayer) => {
        let newTeamList = this.state.teamList.slice();      
            TeamService.getTeamByPlayerId(player._id||'').then((team:ITeam)=>{
                if(!this.checkTeam(team))
                { 
                    if(team._id!=undefined)
                        newTeamList.push(team._id);
                }
                else
                {
                    let index:number=newTeamList.findIndex(item=>item==team._id);
                    newTeamList.splice(index,1);
                }

            });
            this.setState({
                teamList:newTeamList
            });
    }
    public addTeamOrDelete = (recvTeam:ITeam) => {
        let newTeamList = this.state.teamList.slice();      
        TeamService.getTeamById(recvTeam._id||'').then((team:ITeam)=>{
            if(!this.checkTeam(team))
            { 
                if(team._id!=undefined)
                newTeamList.push(team._id);
            }
            else
            {
                let index:number=newTeamList.findIndex(item=>item==team._id);
                newTeamList.splice(index,1);
            }

        });
        this.setState({
            teamList:newTeamList
        });
    }
    public checkTeam(team:ITeam) {
        return this.state.teamList.some((item:String) => team._id === item);
    }
    public deletePlayer=(player:IPlayer)=>
    {
        this.setState(prevState => ({
            playerList: prevState.playerList.filter((el:IPlayer) => el._id != player._id )
        }));
    }
    public deleteTeam=(team:ITeam)=>
    {
        this.setState(prevState => ({
            teamList: prevState.teamList.filter((el:String) => el != team._id )
        }));
    }
    public checkBoxChange()
    {
        this.setState({
            stepInput:!this.state.stepInput
        });
    }
    startGame()
    {
        if(this.state.gameType!=undefined)
        {
            history.replaceState('','',"/play?game="+this.state.gameId+"&type="+this.state.gameType._id+"&foreach="+this.state.stepInput+"&teams="+this.state.teamList.join(','));
            history.go();
        }
        else
        {
            history.replaceState('','','/leaderboard');
            history.go();
        }

    }
    public render() {
        return (
            <div>
                    <div >
                        <h1><button onClick={()=>history.back()} className="btn-back">Back</button></h1>
                        <h1><button onClick={()=>this.startGame()} className="btn-start">Start</button></h1>
                        {this.state.gameType!=undefined&&!!this.state.gameType.allowTeam
                        &&
                        <div>
                            <NavBarTeam handleChange={this.handleChange} selectActivePage={this.state.selectedPage}/>
                        </div>     
                         }
                    </div>
    
                {!!this.state.selectedPage&&
                    <label>
                    <input
                      type="checkbox"
                      name="checkbx_eachplayer"
                      defaultChecked={false}
                      onChange={()=>this.checkBoxChange()}
                      ref="checkbx_eachplayer"
                    disabled/>
                    Points Per Player(trebuie imbunatatit)
                  </label>
                }
                {!!!this.state.selectedPage&&
                    this.state.lobbyPlayer
                }
                {!!this.state.selectedPage&&
                    this.state.lobbyTeam
                }

    
                
                 
            </div>
        );
       
    }
}