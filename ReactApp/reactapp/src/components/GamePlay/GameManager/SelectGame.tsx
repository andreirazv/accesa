import * as React from 'react';
import './SelectGame.css';
import { IGame } from 'src/DataModel/IGame';
import PhotosService from 'src/services/PhotosService';
import { IPhoto } from 'src/DataModel/IPhoto';
declare module '*.png'

export interface ISelectGameProps {
    item: IGame;
    selectGame(item: IGame): void;
    handleChnage(): void;
}
export interface ISelectGameState {
    base64Img:string;
}

export default class SelectGame extends React.Component<ISelectGameProps, ISelectGameState> {
    constructor(props: ISelectGameProps) {
        super(props);
        this.state = {
            base64Img:''
          }
    }
    SelectGame=(game:IGame)=>
    {
        if(game._id!=undefined)
            {
                history.pushState('','','type?game='+game._id);
                history.go();
            }
        else
        {
            alert("There is a problem with selected game!");
        }
    }   
    componentDidMount()
    {
        PhotosService.getPhotoById(this.props.item.gamePhotoId||'').then((image:IPhoto)=>{
            this.setState({
                base64Img:image.sourcePhoto
            });
        });
    }
    render(){
        return (
            
            <span key={this.props.item._id}>
                <button className="select-wrap" onClick={() => {this.SelectGame(this.props.item) }} >
                    <img className="select-img" src={this.state.base64Img} alt={`${this.props.item.gameName}`} />
                        <article className="select-description">
                            <h2>{`${this.props.item.gameName}`}</h2>
                            <p>{`${this.props.item.gameRules}`}</p>
                        </article>
                </button>
            </span>
        );
    }
}

