import * as React from 'react';
import GamesList from '../../ManageGames/GamesList/GamesList';
import SelectGame from './SelectGame';
import './SelectGame.css';
import { Link } from 'react-router-dom';
export interface ISelectGameState {
  gameId?: string;
}

export default class GameList extends React.Component<any, ISelectGameState> {

  constructor(props: any) {
    super(props);

    this.state = {
    }
  }

  public gameSelected = (gameId: string) => {
    this.setState({
      gameId: gameId
    });
  }

  public render() {
    return (
      <div>
        <h2><Link to={'/leaderboard'}><button className="btn-back">Back</button></Link></h2>
        <GamesList onGameSelected={this.gameSelected} selectGame={SelectGame} />
      </div>
    )
  }
}