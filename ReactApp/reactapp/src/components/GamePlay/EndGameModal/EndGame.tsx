import * as React from 'react';
import '../../Login/Login.scss';
import { IMatch } from 'src/DataModel/IMatch';
import { ITeam } from 'src/DataModel/ITeam';
import TeamService from 'src/services/TeamService';
import FileBase64 from 'src/components/ImageDetection/imageToBase64.js';
import { IPhoto } from 'src/DataModel/IPhoto';
import PhotosService from 'src/services/PhotosService';
import MatchService from 'src/services/MatchService';

export interface IEndGameState {
    matchId:string;
    base64Img:string;
    match:IMatch;
    teamId:string;
    team:ITeam;
    file:any;
}
export interface IEndGameProps {
    changeState():void;
}
export default class EndGame extends React.Component<IEndGameProps,IEndGameState > {

  constructor(props: IEndGameProps) {
		super(props);
		const query = new URLSearchParams(location.search);
    this.state = {
            matchId:query.get("match")||'',
            base64Img:'',
            match:Object(null),
            teamId:query.get("winnerTeam")||'',
            team:Object(null),
            file:''
    }
  }
  setFile(file:any){
      console.log("srfile:");
    this.setState({ file: file })
}
  componentWillMount()
    {
        TeamService.getTeamById(this.state.teamId).then((team:ITeam)=>{ 
                this.setState({
                    team:team
                });
        });
    }
    endGame=()=>
    {
        console.log("endsGae"+this.state.file);
        if(this.state.teamId!=undefined)
        {
            const photo:IPhoto={
                sourcePhoto:this.state.file.base64
            };
            const query = new URLSearchParams(location.search);
            console.log("modale"+"match:"+query.get('match'));
            
        
            
            PhotosService.insertPhoto(photo).then((ph:IPhoto)=>{
                MatchService.getMatchById(this.state.matchId).then((rechMatch:IMatch)=>{
                    MatchService.getWinner(query.get('match')||'').then((idteam:string)=>{
                    const newMatch:IMatch={
                        gameId: rechMatch.gameId,
                        typeGameId: rechMatch.typeGameId,
                        teamsAndPoints: rechMatch.teamsAndPoints,
                        winnerTeam: idteam||'',
                        finalPhotoId: ph._id||'',
                    };
                    MatchService.updateMatch(rechMatch._id||'',newMatch).then((newUpMatch:IMatch)=>{
                        history.pushState('','',"play/finished?match="+rechMatch._id+"&finished=true");
                        history.go();
                    });
                });
            });
            });
        }
    }
  public render() {
      console.log("engame");
    return (
    <div className = "bg-opacity">
        <div className="loginpanel">
            <div className="txt">
                <input type="text" placeholder={this.state.team.teamName} name="winner" disabled></input>
            </div>
            <div className="txt">
                <FileBase64
                multiple={ false }
                onDone={ this.setFile.bind(this) } />
            </div>
            <div className="buttons">
                <input onClick={this.endGame} type="button" value="End Game" />
            </div>
        </div>
    </div>
    );
  }
}
