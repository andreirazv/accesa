import * as React from 'react';
import PlayerForm from './AddPlayer/PlayerForm';
import { IPlayer } from 'src/DataModel/IPlayer';
import PlayersService from 'src/services/PlayerService';
import { Link } from 'react-router-dom';


export interface IUpdatePlayerState {
    id?: string;
    PlayerName: string;
    Password: string;
    TotalStatsId: string;
    PlayerPhotoId: string;
    RegisterDate: string;
}
export default class UpdatePlayer extends React.Component<any, IUpdatePlayerState> {

    constructor(props:any) {
        super(props);
        this.state = {
            PlayerName: '',
            Password: '',
            TotalStatsId: '0',
            PlayerPhotoId: '',
            RegisterDate: ''
        }
    }

    componentDidMount() {
        if(this.props.match.params.id !== undefined) {
            PlayersService.getPlayerById(this.props.match.params.id)
                .then((player: IPlayer) => {
                    this.setState({
                        PlayerName: player.playerName,
                        Password: player.password,
                        TotalStatsId: player.totalStatsId,
                        PlayerPhotoId: player.playerPhotoId,
                    });
                });
        } 
    }

    handleChange = (e: any) => {
        const { playerName, value } = e.target;
        this.setState((prevState: IUpdatePlayerState) => (
            { 
                ...prevState,
                [playerName]: value
            }
        ));
    }
    updatePlayer = () => {
        const updatePlayer: IPlayer = {
            playerName: this.state.PlayerName,
            password: this.state.Password,
            totalStatsId: this.state.TotalStatsId,
            playerPhotoId: this.state.PlayerPhotoId,
            registerDate: this.state.RegisterDate
        }
        PlayersService.updatePlayer(this.props.match.params.id, updatePlayer)
            .then(() => {
                this.props.history.push('/manage/player');
            });
    }
    public render() {
        return (
            <div>
                <PlayerForm
                nameAction={"Update"}
                handleChange={this.handleChange}
                playerName={this.state.PlayerName}
                totalStatsId={this.state.TotalStatsId}
                password={this.state.Password}
                registerDate={this.state.RegisterDate}
                playerPhotoId={this.state.PlayerPhotoId}
                />
                <button onClick={this.updatePlayer}>Update Player</button>
                <Link to='/manage/player'><button>Anuleaza</button></Link>
            </div>
        );
    }
}