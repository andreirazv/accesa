import * as React from 'react';
import './NavBarPlayer.css';
import { Link } from 'react-router-dom';
export interface INavBarPlayerState {
}
export interface INavBarPlayerProps{
    handleChange(e:any):void;
    selectActivePage?:boolean;
}

export default class NavBarPlayer extends React.Component<INavBarPlayerProps,INavBarPlayerState> {

  constructor(props: INavBarPlayerProps) {
    super(props);
    
    this.state = {
    }
  }
    public render()
    {
        let allPlayersLink;
        let addButtonLink;
        if(this.props.selectActivePage==false)
        {
            allPlayersLink=<Link className="active" to='/manage/player'>All Players</Link>
            addButtonLink=<Link onClick={this.props.handleChange} to='/manage/player'>Add Player</Link> 
        }
        else
        {
            allPlayersLink=<Link onClick={this.props.handleChange} to='/manage/player'>All Players</Link>
            addButtonLink=<Link className="active" to='/manage/player'>Add Player</Link> 
        }
        return (
            <div>
                <ul className="ul-nav">
                    <li className="li-nav">
                        {allPlayersLink}           
                    </li>
                    <li className="li-nav">
                        {addButtonLink}            
                    </li> 
                </ul>
              </div>
        )
    }
}


