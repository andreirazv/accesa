import * as React from 'react';
import NavBarPlayer from './Navigation/NavBarPlayer';
import PlayersList from './PlayersList/PlayersList';
import AddPlayer from './AddPlayer';
import Player from './PlayersList/Player/Player';
import { IPlayer } from 'src/DataModel/IPlayer';
export interface IPlayerManagerState {
    playerId?: string;
    selectedPage?: boolean;
}
export interface IPlayerManagerProps {
    isAllPlayers?: boolean;
}

export default class PlayerManager extends React.Component<IPlayerManagerProps, IPlayerManagerState> {

  constructor(props: IPlayerManagerProps) {
    super(props);
    this.state = {
        selectedPage:false
    }
  }
  public playerSelected = (player: IPlayer) => {
    this.setState({
      playerId: player._id
    });
  }
public handleChange=()=>
{
    this.setState({
        selectedPage: !this.state.selectedPage
      });
      
}
    public render()
    {
        return (
            <div>
                <NavBarPlayer handleChange={this.handleChange} selectActivePage={this.state.selectedPage}/>
                {!this.state.selectedPage&&<PlayersList onPlayerSelected={this.playerSelected} sPlayer={Player}/>}
                {this.state.selectedPage&&<AddPlayer/>}
                {!!this.state.playerId}
            </div>
        )
    }
}