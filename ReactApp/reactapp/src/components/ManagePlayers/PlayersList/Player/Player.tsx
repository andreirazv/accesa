import * as React from 'react';
import './Player.css';
import { IPlayer } from 'src/DataModel/IPlayer';
import { IStatus } from 'src/DataModel/IStatus';
import StatsService from 'src/services/StatsService';
import PlayerService from 'src/services/PlayerService';
import PhotosService from 'src/services/PhotosService';
import { IPhoto } from 'src/DataModel/IPhoto';

export interface IPlayerProps {
    item: IPlayer;
    selectPlayer(item: IPlayer): void;
}
export interface IPlayerState {
    statistics:IStatus;
    loggedPlayer: boolean;
    base64Img: string;

}
export default class Player extends React.Component<IPlayerProps, IPlayerState> {
    constructor(props: IPlayerProps) {
        super(props);
        this.state = {
            statistics: Object(null),
            loggedPlayer:false,
            base64Img:''
          }
    }
    componentWillMount()
    {
        StatsService.getStatById(this.props.item.totalStatsId).then((stats:IStatus)=>{
            PhotosService.getPhotoById(this.props.item.playerPhotoId).then((photo:IPhoto)=>{
                this.setState({
                    statistics:stats,
                    base64Img:photo.sourcePhoto
                });
            });
        });
        PlayerService.getPlayerById(localStorage.getItem('logged_playerId')||'').then((player:IPlayer)=>{
                if(player._id==this.props.item._id)
                {
                    this.setState({
                        loggedPlayer:true
                    });
                }
        });
    }
    render()
    {
        return (
            <span key={this.props.item._id}>
            {!this.state.loggedPlayer&&
            <button className="player-wrap" onClick={() => {this.props.selectPlayer(this.props.item) }} > 
            <input className="native-hidden" id={`ckbox${this.props.item._id}`} type="checkbox" />
                <label htmlFor={`ckbox${this.props.item._id}`}>
                    <img className="player-img" src={this.state.base64Img} alt={`${this.props.item.playerName}`} />
                    <article className="player-description">
                        <h2>{`${this.props.item.playerName}`}</h2>
                        
                        <p>{`Wins: ${this.state.statistics.wins}`}</p>
                        <p>{`Defeats: ${this.state.statistics.defeats}`}</p>
                        <p>{`Draws: ${this.state.statistics.draws}`}</p>
                    </article> </label>
                    </button>
           }
        </span>
            
        );
    }
}