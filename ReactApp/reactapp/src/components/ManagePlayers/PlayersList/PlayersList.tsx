import * as React from 'react';
import { IPlayer } from 'src/DataModel/IPlayer';
import PlayersService from 'src/services/PlayerService';
import LoadingAnim from 'src/components/Loading/loading';

export interface IPlayersListProps {
    onPlayerSelected(player: IPlayer): void;
    sPlayer: any;
}

export interface IPlayersState {
    players: IPlayer[];
    loading: boolean;
}

export default class PlayersList extends React.Component<IPlayersListProps, IPlayersState> {
    constructor(props: IPlayersListProps) {
        super(props);
        this.state = {
            players: [],
            loading: true
        }
    }

    public componentDidMount() {
        PlayersService.getAllPlayers().then((players: IPlayer[]) => {
            this.setState({
                players: players,
                loading: false
            });
        });
    }

    public selectPlayer = (player: IPlayer) => {
        if (!player._id) return;
        this.props.onPlayerSelected(player);
    }

    public deletePlayer = (player: IPlayer) => {
        if (!player._id) return;
        PlayersService.deletePlayer(player._id)
            .then(() => {
                this.setState((previousState: IPlayersState) => ({
                    players: [...previousState.players.filter(s => s._id !== player._id)]
                }));
            })
    }

    public render() {
        
        return (
            <div>
                <h1>Select your opponents</h1>
                {this.state.loading&&<LoadingAnim/>  }

                {!this.state.loading &&
                    this.state.players.map((item, index) => (
                        <this.props.sPlayer
                            key={index}
                            item={item}
                            selectPlayer={this.selectPlayer}
                            deletePlayer={this.deletePlayer}
                        >
                        </this.props.sPlayer>
                    )
                    )}
            </div>
        );
    }
}