import * as React from 'react';
import { IPlayer } from 'src/DataModel/IPlayer';
import PlayersService from 'src/services/PlayerService';
import PlayerForm from './AddPlayer/PlayerForm';


export interface IAddPlayerState {
    id?: string;
    playerName: string;
    username: string;
    password: string;
    totalStatsId: string;
    playerPhotoId: string;
    registerDate: string;
}
export default class AddPlayer extends React.Component<any, IAddPlayerState> {

    constructor(props:any) {
        super(props);
        this.state = {
            playerName: '',
            username: '',
            password: '',
            totalStatsId: '',
            playerPhotoId: '',
            registerDate: ''
        }
    }
    handleChange = (e: any) => {
        const { playerName, value } = e.target;
        this.setState((prevState: IAddPlayerState) => (
            { 
                ...prevState,
                [playerName]: value
            }
        ));
    }
    addPlayer = () => {
        const newPlayer: IPlayer = {
            playerName: this.state.playerName,
            username: this.state.username,
            password: this.state.password,
            totalStatsId: this.state.totalStatsId,
            playerPhotoId: this.state.playerPhotoId,
        };
        PlayersService.insertPlayer(newPlayer)
            .then(() => {
            });
    }
    public render() {
        return (
            <div>
                <PlayerForm
                handleChange={this.handleChange}
                playerName={this.state.playerName}
                totalStatsId={this.state.totalStatsId}
                password={this.state.password}
                username={this.state.username}
                registerDate={this.state.registerDate}
                playerPhotoId={this.state.playerPhotoId}

                nameAction={"Add"}
                />
                {
                    <button onClick={this.addPlayer}>Add Player</button>
                }
                
            </div>
        );
    }
}