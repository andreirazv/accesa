import * as React from 'react';

export interface IPlayerFormProps {
    handleChange(e: any): void;
    playerName: string;
    username?: string;
    password: string;
    totalStatsId: string;
    playerPhotoId: string;
    registerDate: string;
    nameAction: string;
}

const PlayerForm = (props: IPlayerFormProps) => {
    return (
        <div>
                <form action="/">
                <h1>{props.nameAction} Manager</h1>
                <label htmlFor="player">User Name</label>
                <input type="text" name="player" onChange={props.handleChange} value={props.playerName}/><br /><br />
                <label htmlFor="userName">Player Name</label>
                <input type="text" name="userName" onChange={props.handleChange} value={props.username}/><br /><br />
                       
                <label htmlFor="pass">Password</label>
                <input type="text" name="pass" onChange={props.handleChange} value={props.password}/><br /><br />
                       
                <label htmlFor="stats">TotalStatsId</label>
                <input type="text" name="stats" onChange={props.handleChange} value={props.totalStatsId}/><br /><br />
                       
                <label htmlFor="photoid">PlayerPhotoId</label>
                <input type="text" name="photoid" onChange={props.handleChange} value={props.playerPhotoId}/><br /><br />
                       
                <label htmlFor="register">RegisterDate</label>
                <input type="text" name="register" onChange={props.handleChange} value={props.registerDate}/><br /><br />
                       
            </form>
        </div>
    );
}

export default PlayerForm;