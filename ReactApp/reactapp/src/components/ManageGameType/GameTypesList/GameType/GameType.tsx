import * as React from 'react';
import './GameType.css';
import { Link } from 'react-router-dom';
import { IGameType } from 'src/DataModel/IGameType';


export interface IGameTypeProps {
    item: IGameType;
    selectGameType(item: IGameType): void;
    deleteGameType(item: IGameType): void;
}

const GameType = (props: IGameTypeProps) => {
    return (
        <div key={props.item._id} >
            <span className='gameType' onClick={() => { props.selectGameType(props.item) }}>{`${props.item.typeName} - ${props.item.countPlayed}`}</span>
            <button><Link to={`/manage/gametype/${props.item._id}`}>Update</Link></button>
            <button onClick={() => props.deleteGameType(props.item)}>Delete</button>
        </div>
    );
}

export default GameType;