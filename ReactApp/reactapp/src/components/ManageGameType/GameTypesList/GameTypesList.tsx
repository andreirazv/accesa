import * as React from 'react';
import { IGameType } from 'src/DataModel/IGameType';
import GameTypesService from 'src/services/GameTypeService';
import LoadingAnim from 'src/components/Loading/loading';
import './GameTypeList.css';

export interface IGameTypeListProps {
    onGameTypeSelected(id: string): void;
    selectType: any;
}

export interface IGameTypeState {
    gameTypes: IGameType[];
    loading: boolean;
}

export default class GameTypesList extends React.Component<IGameTypeListProps, IGameTypeState> {
    constructor(props: IGameTypeListProps) {
        super(props);
        this.state = {
            gameTypes: [],
            loading: true
        }
    }

    public componentDidMount() {
        GameTypesService.getAllGameTypes().then((gameTypes: IGameType[]) => {
            this.setState({
                gameTypes: gameTypes,
                loading: false
            });
        });
    }

    public selectGameType = (gameType: IGameType) => {
        if (!gameType._id) return;
        this.props.onGameTypeSelected(gameType._id);
    }

    public deleteGameType = (gameType: IGameType) => {
        if (!gameType._id) return;
        GameTypesService.deleteGameType(gameType._id)
            .then(() => {
                this.setState((previousState: IGameTypeState) => ({
                    gameTypes: [...previousState.gameTypes.filter(s => s._id !== gameType._id)]
                }));
            })
    }

    public render() {

        return (
            <div className="bg-types">
                <h1>Select Type</h1>
                {this.state.loading&&<LoadingAnim/>  }

                {!this.state.loading &&
                    this.state.gameTypes.map((item, index) => (
                        <this.props.selectType
                            key={index}
                            item={item}
                            selectGameType={this.selectGameType}
                            deleteGameType={this.deleteGameType}
                        >
                        </this.props.selectType>
                    )
                    )}
            </div>
        );
    }
}