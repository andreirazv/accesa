import * as React from 'react';
import GameTypesList from './GameTypesList/GameTypesList';
import AddGameType from './AddGameType';
import NavBarGameType from './Navigation/NavBarGameType';
import GameType from './GameTypesList/GameType/GameType';
export interface IGameTypeManagerState {
    gameTypeId?: string;
    selectedPage?: boolean;
}
export interface IGameTypeManagerProps {
    isAllGameTypes?: boolean;
}

export default class GameTypeManager extends React.Component<IGameTypeManagerProps, IGameTypeManagerState> {

  constructor(props: IGameTypeManagerProps) {
    super(props);
    this.state = {
        selectedPage:false
    }
  }
  public gameTypeSelected = (gameTypeId: string) => {
    this.setState({
      gameTypeId: gameTypeId
    });
  }
public handleChange=()=>
{
    this.setState({
        selectedPage: !this.state.selectedPage
      });
      
}
    public render()
    {
        let pageRender;
        if(this.state.selectedPage==false)
        {
            pageRender=<GameTypesList onGameTypeSelected={this.gameTypeSelected} selectType={GameType}/>
        }
        else
        {
            pageRender=<AddGameType/>
        }
        return (
            <div>
                <NavBarGameType handleChange={this.handleChange} selectActivePage={this.state.selectedPage}/>
                {pageRender}
                {!!this.state.gameTypeId}
            </div>
        )
    }
}