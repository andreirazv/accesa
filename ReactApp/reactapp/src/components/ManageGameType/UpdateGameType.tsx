import * as React from 'react';
import { IGameType } from 'src/DataModel/IGameType';
import { Link } from 'react-router-dom';
import GameTypeService from 'src/services/GameTypeService';
import GameTypeForm from './AddGameType/GameTypeForm';

export interface IUpdateGameTypeState {
    id?: string;
    typename: string;
    allowteam: boolean;
    remianing: number;
    time: number;
    rounds: number;
    countPlayed: number;
}
export default class UpdateGameType extends React.Component<any, IUpdateGameTypeState> {

    constructor(props:any) {
        super(props);
        this.state = {
            typename: '',
            allowteam: false,
            remianing: 0,
            time: 0,
            rounds: 0,
            countPlayed: 0,
        }
        this.updateGameType=this.updateGameType.bind(this);
    }

    componentDidMount() {
        if(this.props.match.params.id !== undefined) {
            GameTypeService.getGameTypeById(this.props.match.params.id)
                .then((gameType: IGameType) => {
                    this.setState({
                        typename: gameType.typeName,
                        allowteam: gameType.allowTeam,
                        remianing: gameType.remainingPoints,
                        time: gameType.time,
                        rounds: gameType.rounds,
                        countPlayed: gameType.countPlayed
                    });
                });
        } 
    }

    handleChange = (e: any) => {
        const { typeName, value } = e.target;
        this.setState((prevState: IUpdateGameTypeState) => ({
            ...prevState,
            [typeName]: value
        }));
    }
    updateGameType = () => {
        const updateGameType: IGameType = {
            typeName: this.state.typename,
            allowTeam: this.state.allowteam,
            remainingPoints: this.state.remianing,
            rounds: this.state.rounds,
            time: this.state.time,
            countPlayed: this.state.countPlayed
        }
        GameTypeService.updateGameType(this.props.match.params.id, updateGameType)
            .then(() => {
                this.props.history.push('/manage/gametype');
            });
    }
    public render() {
        return (
            <div className="">
                <GameTypeForm
                nameAction={"Update"}
                typename={this.state.typename}
                allowteam={Boolean(this.state.allowteam)}
                time={this.state.time}
                rounds={this.state.rounds}
                remianing={this.state.remianing}
                addType={this.updateGameType}
                handleChange={this.handleChange}/>
                <Link to='/manage/gametype'><button>Anuleaza</button></Link>
            </div>
        );
    }
}