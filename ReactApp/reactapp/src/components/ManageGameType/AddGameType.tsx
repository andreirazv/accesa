import * as React from 'react';
import { IGameType } from 'src/DataModel/IGameType';
import GameTypesService from 'src/services/GameTypeService';
import AddGameTypeForm from './AddGameType/GameTypeForm';


export interface IAddGameTypeState {
    id?: string;
    typename: string;
    allowteam: boolean;
    remianing: number;
    time: number;
    rounds: number;
    countPlayed: number;
}
export default class AddGameType extends React.Component<any, IAddGameTypeState> {

    constructor(props:any) {
        super(props);
        this.state = {
            typename: '',
            allowteam: false,
            remianing: 0,
            time: 0,
            rounds: 0,
            countPlayed: 0,
        }
        this.addGameType=this.addGameType.bind(this);
    }
    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IAddGameTypeState) => (
            { 
                ...prevState,
                [name]: value
            }
        ));
    }
    addGameType () {
        const newGameType: IGameType = {
            typeName: this.state.typename,
            allowTeam: this.state.allowteam,
            remainingPoints: this.state.remianing,
            time: this.state.time,
            rounds: this.state.rounds,
            countPlayed: this.state.countPlayed
        };
        GameTypesService.insertGameType(newGameType)
            .then(() => {
            });
    }
    public render() {
        return (
            <div>
                <AddGameTypeForm
                typename={this.state.typename}
                allowteam={Boolean(this.state.allowteam)}
                time={this.state.time}
                rounds={this.state.rounds}
                remianing={this.state.remianing}
                nameAction={"Add"}
                addType={this.addGameType}
                handleChange={this.handleChange}/>

            </div>
        );
    }
}