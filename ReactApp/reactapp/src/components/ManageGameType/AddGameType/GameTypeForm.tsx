import * as React from 'react';
import '../../ManageGames/AddGame/GameForm.css';
export interface IGameTypeFormProps {
    handleChange(e: any): void;
    typename: string;
    remianing: number;
    rounds: number;
    time: number;
    allowteam: boolean;
    nameAction: string;
    addType(): void;
}

const GameTypeForm = (props: IGameTypeFormProps) => {
    return (
        
        <div className="add-game">
                <form action="/">
                <h1>{props.nameAction} Manager</h1>
                <label htmlFor="typename">GameType Name: </label>
                <input type="text" name="typename" onChange={props.handleChange} value={props.typename}/><br /><br />
                       
                <label htmlFor="remianing">Remianing Points</label>
                <input type="text" name="remianing" onChange={props.handleChange} value={props.remianing}/><br /><br />
                       
                <label htmlFor="time">Time: </label>
                <input type="text" name="time" onChange={props.handleChange} value={props.time}/><br /><br />
                       
                <label htmlFor="rounds">Rounds: </label>
                <input type="text" name="rounds" onChange={props.handleChange} value={props.rounds}/><br /><br />
                
                <label htmlFor="allowteam">Allow Team: </label>
                <input type="text" name="allowteam" onClick={props.handleChange} value={String(!props.allowteam)}/><br /><br />
                <button onClick={props.addType}>Add GameType</button>

            </form>
        </div>
    );
}

export default GameTypeForm;