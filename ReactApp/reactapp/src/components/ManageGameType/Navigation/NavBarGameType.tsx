import * as React from 'react';
import './NavBarGameType.css';
import { Link } from 'react-router-dom';
export interface INavBarGameTypeState {
}
export interface INavBarGameTypeProps{
    handleChange(e:any):void;
    selectActivePage?:boolean;
}

export default class NavBarGameType extends React.Component<INavBarGameTypeProps,INavBarGameTypeState> {

  constructor(props: INavBarGameTypeProps) {
    super(props);
    
    this.state = {
    }
  }
    public render()
    {
        let allGameTypesLink;
        let addButtonLink;
        if(this.props.selectActivePage==false)
        {
            allGameTypesLink=<Link className="active" to='/manage/gametype'>All GameTypes</Link>
            addButtonLink=<Link onClick={this.props.handleChange} to='/manage/gametype'>Add GameType</Link> 
        }
        else
        {
            allGameTypesLink=<Link onClick={this.props.handleChange} to='/manage/gametype'>All GameTypes</Link>
            addButtonLink=<Link className="active" to='/manage/gametype'>Add GameType</Link> 
        }
        return (
            <div>
                <ul className="ul-nav">
                    <li className="li-nav">
                        {allGameTypesLink}           
                    </li>
                    <li className="li-nav">
                        {addButtonLink}            
                    </li> 
                </ul>
              </div>
        )
    }
}


