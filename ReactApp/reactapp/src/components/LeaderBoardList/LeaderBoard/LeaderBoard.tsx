import * as React from 'react';
import './LeaderBoard.css';
import { ILeaderBoard } from 'src/DataModel/ILeaderBoard';

export interface ILeaderBoardProps {
    item: ILeaderBoard;
}

const leaderboard = (props:ILeaderBoardProps) => {
    return (
        <div key={props.item._id}>
         <table id="customers">   
         <tbody>
         <tr> 
            <td className="item-data-rank">{`${props.item.position}`}</td>
            <td className="item-data">{`${props.item.name}`}</td>
            <td className="item-data">{`${props.item.wins}`}</td>
            <td className="item-data">{`${props.item.defeats}`}</td>
            <td className="item-data">{`${props.item.draws}`}</td>
            </tr>
            </tbody>
</table>
        </div>
    );
}

export default leaderboard;




