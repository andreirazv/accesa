import * as React from 'react';
import LeaderBoard from './LeaderBoard/LeaderBoard';
import { ILeaderBoard } from 'src/DataModel/ILeaderBoard';
import './LeaderBoard/LeaderBoard.css';


export interface ILeaderBoardListProps {
    players: ILeaderBoard[];
}

export default class LeaderBoardList extends React.Component<ILeaderBoardListProps, any> {

    constructor(props:ILeaderBoardListProps) {
        super(props);
    }
    public render() {

        return (
            <div className="rectangle-box-rank">
                <h2 className="title-text">Player Ranking</h2>
                <table id="customers">
                <thead>
                    <tr>
                      <th className="collumn-data">Rank</th>
                      <th className="collumn-data">Name</th>
                      <th className="collumn-data">Wins</th>
                      <th className="collumn-data">Defeats</th>
                      <th className="collumn-data">Draws</th>
                      </tr>
                    </thead>
                    
                  </table>
                {
                   
                    this.props.players.map((item, index) => (
                        <LeaderBoard item={item} key={index}/> 
                    ))}
                    <br/>
            </div>
        );
    }
}