import * as React from 'react';
import Modal from 'react-modal';

export interface IModalState {
  showModal: boolean;
}
export interface IModalProps {
 showModal():void;
  component:any;
}

export default class ModalForm extends React.Component<IModalProps, IModalState> {

  constructor(props: any) {
    super(props);
    this.state = {
      showModal: true
    }
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
  }

  public handleCloseModal () {
    this.props.showModal();
    this.setState({ showModal: false });
  }
  public handleOpenModal () {
    this.setState({ showModal: true });
  }
  public modalState () {
    const stateRef = this.state;
    return stateRef["showModal"];
  }

  public render () {
    return (
      <span>
          <this.props.component changeState={this.handleCloseModal}/>  
      </span>
    );
  }
}
Modal.setAppElement('#root');
