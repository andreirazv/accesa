import * as React from 'react';
import { Link } from 'react-router-dom';
import './NavBeforeLogin.css'
export interface INavigationBeforeState {
    logoImage: string;
}
export default class NavBeforeLogin extends React.Component<any, INavigationBeforeState> {

    constructor(props: any) {
        super(props);
        this.state = {
            logoImage: ''
          }
    }
    componentWillMount()
    {
       this.setState({
           logoImage : "/Resources/gamepad.png"
        })
    }
    public render() {
       
        return (
            <div>
                <nav>
                    <ul className="ul-style">
                        <li className="li-style">
                            <h1>
                            <Link to='/'>Game&nbsp;<img className="image-style" src={this.state.logoImage}>
                            </img>&nbsp;Room</Link>
                            <br />
                            </h1>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}