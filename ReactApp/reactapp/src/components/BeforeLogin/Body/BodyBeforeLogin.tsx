import * as React from 'react';
import './BodyBeforeLogin.css'
import ModalForm from 'src/components/ModalForm/ModalForm';
import ModalLogin from '../modalLogin/ModalLogin';

export interface ILoginState {
    showModal?: boolean;
  }
export default class BodyBeforeLogin extends React.Component<ILoginState,any> {

    constructor(props:any) {
        super(props);
        this.state = {
            showModal: false,
        };
        this.changeModalState = this.changeModalState.bind(this);
      }

    changeModalState() {
        this.setState({
            showModal: !this.state.showModal,
        });
      }
      
    public render() {
        return (
            <div className="rectangle">

                <button onClick={this.changeModalState} className="button-ul">                    
                    <div className="text-button">Let's Play!</div>
                </button>
                {this.state.showModal &&<ModalForm component={ModalLogin} showModal = {this.changeModalState}/>}
            </div>
        )
    }
}