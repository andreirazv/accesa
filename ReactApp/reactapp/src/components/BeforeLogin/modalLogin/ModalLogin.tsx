import * as React from 'react';
import { Login } from 'src/components/Login/login';
import { Register } from 'src/components/Login/Register';

export interface IModalProps {
    changeState():void;
}
export interface IModalState
{
  isLogin:boolean;
}
export default class ModalLogin extends React.Component<IModalProps, IModalState> {

  constructor(props: IModalProps) {
    super(props);
    this.state = {
      isLogin:true
    }
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.changeView = this.changeView.bind(this);
  }

  public handleCloseModal () {
   this.props.changeState();
  }
  changeView()
  {
    this.setState({isLogin:!this.state.isLogin})
  }
  public render () {
    return (
      <div>
        {!!this.state.isLogin&&
      <Login changeView={this.changeView} handleCloseModal={()=>this.handleCloseModal}/>
        }
        {!!!this.state.isLogin&&
      <Register changeView={this.changeView} handleCloseModal={()=>this.handleCloseModal}/>
        }
      </div>
    );
  }
}
