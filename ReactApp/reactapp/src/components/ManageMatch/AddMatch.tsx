import * as React from 'react';
import { IMatch } from 'src/DataModel/IMatch';
import MatchsService from 'src/services/MatchService';
import MatchForm from './AddMatch/MatchForm';


export interface IAddMatchState {
    id?: string;
    gameId: string;
    typeGameId: string;
    winnerTeam: string;
    finalPhotoId: string;
}
export default class AddMatch extends React.Component<any, IAddMatchState> {

    constructor(props:any) {
        super(props);
        this.state = {
            gameId: '',
            typeGameId: '',
            winnerTeam: '',
            finalPhotoId: ''
        }
    }
    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IAddMatchState) => (
            { 
                ...prevState,
                [name]: value
            }
        ));
    }
    addMatch = () => {
        const newMatch: IMatch = {
            gameId: this.state.gameId,
            typeGameId: this.state.typeGameId,
            winnerTeam: this.state.winnerTeam,
            finalPhotoId: this.state.finalPhotoId
        };
        MatchsService.insertMatch(newMatch)
            .then(() => {
            });
    }
    public render() {
        return (
            <div>
                <MatchForm
                nameAction={"Add"}
                gameId={this.state.gameId}
                typeGameId={this.state.typeGameId}
                winnerTeam={this.state.winnerTeam}
                finalPhotoId={this.state.finalPhotoId}
                handleChange={this.handleChange}/>
                {
                    <button onClick={this.addMatch}>Add Match</button>
                }
                
            </div>
        );
    }
}