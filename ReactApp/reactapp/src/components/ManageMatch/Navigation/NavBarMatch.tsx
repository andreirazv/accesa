import * as React from 'react';
import './NavBarMatch.css';
import { Link } from 'react-router-dom';
export interface INavBarMatchState {
}
export interface INavBarMatchProps{
    handleChange(e:any):void;
    selectActivePage?:boolean;
}

export default class NavBarMatch extends React.Component<INavBarMatchProps,INavBarMatchState> {

  constructor(props: INavBarMatchProps) {
    super(props);
    
    this.state = {
    }
  }
    public render()
    {
        let allMatchsLink;
        let addButtonLink;
        if(this.props.selectActivePage==false)
        {
            allMatchsLink=<Link className="active" to='/manage/match'>All Matchs</Link>
            addButtonLink=<Link onClick={this.props.handleChange} to='/manage/match'>Add Match</Link> 
        }
        else
        {
            allMatchsLink=<Link onClick={this.props.handleChange} to='/manage/match'>All Matchs</Link>
            addButtonLink=<Link className="active" to='/manage/match'>Add Match</Link> 
        }
        return (
            <div>
                <ul className="ul-nav">
                    <li className="li-nav">
                        {allMatchsLink}           
                    </li>
                    <li className="li-nav">
                        {addButtonLink}            
                    </li> 
                </ul>
              </div>
        )
    }
}


