import * as React from 'react';
import MatchForm from './AddMatch/MatchForm';
import { IMatch } from 'src/DataModel/IMatch';
import MatchsService from 'src/services/MatchService';
import { Link } from 'react-router-dom';


export interface IUpdateMatchState {
    id?: string;
    gameId: string;
    typeGameId: string;
    winnerTeam: string;
    finalPhotoId: string;
}
export default class UpdateMatch extends React.Component<any, IUpdateMatchState> {

    constructor(props:any) {
        super(props);
        this.state = {
            gameId: '',
            typeGameId: '',
            winnerTeam: '',
            finalPhotoId: ''
        }
    }

    componentDidMount() {
        if(this.props.match.params.id !== undefined) {
            MatchsService.getMatchById(this.props.match.params.id)
                .then((match: IMatch) => {
                    this.setState({
                        gameId: match.gameId,
                        typeGameId: match.typeGameId,
                        winnerTeam: match.winnerTeam,
                        finalPhotoId: match.finalPhotoId                    });
                });
        } 
    }

    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IUpdateMatchState) => (
            { 
                ...prevState,
                [name]: value
            }
        ));
    }
    updateMatch = () => {
        const updateMatch: IMatch = {
            gameId: this.state.gameId,
            typeGameId: this.state.typeGameId,
            winnerTeam: this.state.winnerTeam,
            finalPhotoId: this.state.finalPhotoId
        }
        MatchsService.updateMatch(this.props.match.params.id, updateMatch)
            .then(() => {
                this.props.history.push('/manage/match');
            });
    }
    public render() {
        return (
            <div>
                <MatchForm
                nameAction={"Update"}
                gameId={this.state.gameId}
                typeGameId={this.state.typeGameId}
                winnerTeam={this.state.winnerTeam}
                finalPhotoId={this.state.finalPhotoId}
                handleChange={this.handleChange}/>
                <button onClick={this.updateMatch}>Update Match</button>
                <Link to='/manage/match'><button>Anuleaza</button></Link>
            </div>
        );
    }
}