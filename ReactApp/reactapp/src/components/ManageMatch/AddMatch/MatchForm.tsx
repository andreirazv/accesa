import * as React from 'react';

export interface IMatchFormProps {
    handleChange(e: any): void;
    gameId: string;
    typeGameId: string;
    winnerTeam: string;
    finalPhotoId: string;
    nameAction: string;
}

const MatchForm = (props: IMatchFormProps) => {
    return (
        <div>
                <form action="/">
                <h1>{props.nameAction} Manager</h1>
                <label htmlFor="gameid">Game Id Name</label>
                <input type="text" name="gameid" onChange={props.handleChange} value={props.gameId}/><br /><br />
                       
                <label htmlFor="type">Type Id Game</label>
                <input type="text" name="type" onChange={props.handleChange} value={props.typeGameId}/><br /><br />
         
             <br /><br />
                <label htmlFor="winner">winnerTeam</label>
                <input type="text" name="winner" onChange={props.handleChange} value={props.winnerTeam}/><br /><br />
                       
                <label htmlFor="finalPhoto">finalPhotoId</label>
                <input type="text" name="finalPhoto" onChange={props.handleChange} value={props.finalPhotoId}/><br /><br />
                       
                      
            </form>
        </div>
    );
}

export default MatchForm;