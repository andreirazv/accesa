import * as React from 'react';
import NavBarMatch from './Navigation/NavBarMatch';
import AddMatch from './AddMatch';
import MatchsList from './MatchList/MatchList';
import Match from './MatchList/Match/Match';
export interface IMatchManagerState {
    matchId?: string;
    selectedPage?: boolean;
}
export interface IMatchManagerProps {
    isAllMatchs?: boolean;
}

export default class MatchManager extends React.Component<IMatchManagerProps, IMatchManagerState> {

  constructor(props: IMatchManagerProps) {
    super(props);
    this.state = {
        selectedPage:false
    }
  }
  public matchSelected = (matchId: string) => {
    this.setState({
      matchId: matchId
    });
  }
public handleChange=()=>
{
    this.setState({
        selectedPage: !this.state.selectedPage
      });
      
}
    public render()
    {
        let pageRender;
        if(this.state.selectedPage==false)
        {
            pageRender=<MatchsList onMatchSelected={this.matchSelected} sMatch={Match}/>
        }
        else
        {
            pageRender=<AddMatch/>
        }
        return (
            <div>
                <NavBarMatch handleChange={this.handleChange} selectActivePage={this.state.selectedPage}/>
                {pageRender}
                {!!this.state.matchId}
            </div>
        )
    }
}