import * as React from 'react';
import './Match.css';
import { Link } from 'react-router-dom';
import { IMatch } from 'src/DataModel/IMatch';


export interface IMatchProps {
    item: IMatch;
    selectMatch(item: IMatch): void;
    deleteMatch(item: IMatch): void;
}

const Match = (props: IMatchProps) => {
    return (
        <div key={props.item._id} >
            <span className='match' onClick={() => { props.selectMatch(props.item) }}>{`${props.item.gameId} - ${props.item.typeGameId}`}</span>
            <button><Link to={`/manage/match/${props.item._id}`}>Update</Link></button>
            <button onClick={() => props.deleteMatch(props.item)}>Delete</button>
        </div>
    );
}

export default Match;