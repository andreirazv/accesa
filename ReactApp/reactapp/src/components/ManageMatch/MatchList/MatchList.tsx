import * as React from 'react';
import { IMatch } from 'src/DataModel/IMatch';
import MatchsService from 'src/services/MatchService';
import LoadingAnim from 'src/components/Loading/loading';

export interface IMatchsListProps {
    onMatchSelected(id: string): void;
    sMatch: any;
}

export interface IMatchsState {
    matchs: IMatch[];
    loading: boolean;
}

export default class MatchsList extends React.Component<IMatchsListProps, IMatchsState> {
    constructor(props: IMatchsListProps) {
        super(props);
        this.state = {
            matchs: [],
            loading: true
        }
    }

    public componentDidMount() {
        MatchsService.getAllMatchs().then((matchs: IMatch[]) => {
            this.setState({
                matchs: matchs,
                loading: false
            });
        });
    }

    public selectMatch = (match: IMatch) => {
        if (!match._id) return;
        this.props.onMatchSelected(match._id);
    }

    public deleteMatch = (match: IMatch) => {
        if (!match._id) return;
        MatchsService.deleteMatch(match._id)
            .then(() => {
                this.setState((previousState: IMatchsState) => ({
                    matchs: [...previousState.matchs.filter(s => s._id !== match._id)]
                }));
            })
    }

    public render() {

        return (
            <div>
                <h1>Matchs List</h1>
                {this.state.loading&&<LoadingAnim/>  }

                {!this.state.loading &&
                    this.state.matchs.map((item, index) => (
                        <this.props.sMatch
                            key={index}
                            item={item}
                            selectMatch={this.selectMatch}
                            deleteMatch={this.deleteMatch}
                        >
                        </this.props.sMatch>
                    )
                    )}
            </div>
        );
    }
}