import * as React from 'react';
import { IGame } from 'src/DataModel/IGame';
import GamesService from 'src/services/GameService';


export interface IUpdateGameState {
    id?: string;
    name: string;
    rules: string;
    photo: string;
    countPlayed: string;
}
export default class UpdateGame extends React.Component<any, IUpdateGameState> {

    constructor(props:any) {
        super(props);
        this.state = {
            name: '',
            rules: '',
            countPlayed: '0',
            photo: ''
        }
    }

    componentDidMount() {
        if(this.props.match.params.id !== undefined) {
            GamesService.getGameById(this.props.match.params.id)
                .then((game: IGame) => {
                    this.setState({
                        name: game.gameName,
                        rules: game.gameRules,
                        countPlayed: game.countPlayed,
                    });
                });
        } 
    }

    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IUpdateGameState) => (
            { 
                ...prevState,
                [name]: value
            }
        ));
    }
    updateGame = () => {
        const updateGame: IGame = {
            gameName: this.state.name,
            gamePhotoId: this.state.photo,
            gameRules: this.state.rules,
            countPlayed: this.state.countPlayed
        }
        GamesService.updateGame(this.props.match.params.id, updateGame)
            .then(() => {
                this.props.history.push('/manage/game');
            });
    }
    public render() {
       /* return (
            <div>
               <GameForm
                nameAction={"Update"}
                name={this.state.name}
                rules={this.state.rules}
                photo={this.state.photo}
                handleChange={this.handleChange}/>
                <button onClick={this.updateGame}>Update Game</button>
                <Link to='/manage/game'><button>Anuleaza</button></Link>
            </div>
        );*/return null;
    }
}