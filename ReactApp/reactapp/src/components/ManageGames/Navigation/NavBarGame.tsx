import * as React from 'react';
import './NavBarGame.css';
import { Link } from 'react-router-dom';
export interface INavBarGameState {
}
export interface INavBarGameProps{
    handleChange(e:any):void;
    selectActivePage?:boolean;
}

export default class NavBarGame extends React.Component<INavBarGameProps,INavBarGameState> {

  constructor(props: INavBarGameProps) {
    super(props);
    
    this.state = {
    }
  }
    public render()
    {
        let allGamesLink;
        let addButtonLink;
        if(this.props.selectActivePage==false)
        {
            allGamesLink=<Link className="active" to='/manage/game'>All Games</Link>
            addButtonLink=<Link onClick={this.props.handleChange} to='/manage/game'>Add Game</Link> 
        }
        else
        {
            allGamesLink=<Link onClick={this.props.handleChange} to='/manage/game'>All Games</Link>
            addButtonLink=<Link className="active" to='/manage/game'>Add Game</Link> 
        }
        return (
            <div>
                <ul className="ul-nav">
                    <li className="li-nav">
                        {allGamesLink}           
                    </li>
                    <li className="li-nav">
                        {addButtonLink}            
                    </li> 
                </ul>
              </div>
        )
    }
}


