import * as React from 'react';
import './Game.css';
import { Link } from 'react-router-dom';
import { IGame } from 'src/DataModel/IGame';


export interface IGameProps {
    item: IGame;
    selectGame(item: IGame): void;
    deleteGame(item: IGame): void;
}

const Game = (props: IGameProps) => {
    return (
        <div key={props.item._id} >
            <span className='game' onClick={() => { props.selectGame(props.item) }}>{`${props.item.gameName} - ${props.item.countPlayed}`}</span>
            <button><Link to={`/manage/game/${props.item._id}`}>Update</Link></button>
            <button onClick={() => props.deleteGame(props.item)}>Delete</button>
        </div>
    );
}

export default Game;