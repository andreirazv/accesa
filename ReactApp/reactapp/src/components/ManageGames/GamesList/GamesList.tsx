import * as React from 'react';
import { IGame } from 'src/DataModel/IGame';
import GamesService from 'src/services/GameService';
import LoadingAnim from 'src/components/Loading/loading';
import './Game/Game.css';

export interface IGamesListProps {
    onGameSelected(id: string): void;
    selectGame: any;
}

export interface IGamesState {
    games: IGame[];
    loading: boolean;
}

export default class GamesList extends React.Component<IGamesListProps, IGamesState> {
    constructor(props: IGamesListProps) {
        super(props);
        this.state = {
            games: [],
            loading: true
        }
    }

    public componentDidMount() {
        GamesService.getAllGames().then((games: IGame[]) => {
            this.setState({
                games: games,
                loading: false
            });
        });
    }

    public selectGame = (game: IGame) => {
        if (!game._id) return;
        this.props.onGameSelected(game._id);
    }

    public deleteGame = (game: IGame) => {
        if (!game._id) return;
        GamesService.deleteGame(game._id)
            .then(() => {
                this.setState((previousState: IGamesState) => ({
                    games: [...previousState.games.filter(s => s._id !== game._id)]
                }));
            })
    }

    public render() {

        return (
            <div className="bg-games">
                <h1>Select Game</h1>
                {this.state.loading&&<LoadingAnim/>  }
                
                {!this.state.loading &&
                    this.state.games.map((item, index) => (
                        
                        <this.props.selectGame
                            key={index}
                            item={item}
                            selectGame={this.selectGame}
                            deleteGame={this.deleteGame}
                        >
                        </this.props.selectGame>
                         
                    )
                    )}
                   
            </div>
        );
    }
}