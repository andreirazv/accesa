import * as React from 'react';
import { IGame } from 'src/DataModel/IGame';
import GamesService from 'src/services/GameService';
import GameForm from './AddGame/GameForm';
import PhotosService from 'src/services/PhotosService';
import { IPhoto } from 'src/DataModel/IPhoto';

export interface IAddGameState {
    id?: string;
    name: string;
    rules: string;
    photo: string;
    countPlayed: string;
    file: string;
}
export default class AddGame extends React.Component<any, IAddGameState> {

    constructor(props:any) {
        super(props);
        this.state = {
            name: '',
            rules: '',
            file:'',
            countPlayed: '0',
            photo: ''
        }
        this.getFiles=this.getFiles.bind(this);
        this.addGame=this.addGame.bind(this);
    }
    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IAddGameState) => (
            { 
                ...prevState,
                [name]: value
            }
        ));
    }
    addGame() {
        
        const newPhoto: IPhoto = {
           sourcePhoto:this.state.file
        };
        PhotosService.insertPhoto(newPhoto).then((photo:IPhoto)=>{
            const newGame: IGame = {
                gameName: this.state.name,
                gameRules: this.state.rules,
                countPlayed: this.state.countPlayed,
                gamePhotoId:photo._id
            };
        
        GamesService.insertGame(newGame)
            .then(() => {
            });
        });
    }
    getFiles(files:any){
        this.setState({ file: files })
    }
    public render() {
        return (
            <div>

                <GameForm
                name={this.state.name}
                nameAction={"Add"}
                rules={this.state.rules}
                getFiles={this.getFiles}
                handleChange={this.handleChange}
                addGame = {this.addGame}/>
                
            </div>
        );
    }
}