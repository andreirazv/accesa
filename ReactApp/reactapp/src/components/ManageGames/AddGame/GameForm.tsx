import * as React from 'react';
import FileBase64 from 'src/components/ImageDetection/imageToBase64.js';
import './GameForm.css';
export interface IGameFormProps {
    handleChange(e: any): void;
    name: string;
    rules: string;
    getFiles(file:any): void;
    nameAction: string;
    addGame(): void;
}

const GameForm = (props: IGameFormProps) => {
    return (

        <div className="add-game">
                <form action="/">
                <h1>{props.nameAction} Game</h1>
                <label htmlFor="name">Game Name: </label>
                <input type="text" name="name" onChange={props.handleChange} value={props.name}/><br /><br />
                       
                 <span>Game Photo: <FileBase64
                    multiple={ false }
                    onDone={ props.getFiles } /></span><br /><br />
                <label htmlFor="rules">Game Rules: </label>
                <textarea name="rules" onChange={props.handleChange} value={props.rules}/><br /><br />
                <button onClick={props.addGame}>Add Game</button>
            </form>
        </div>
    );
}

export default GameForm;