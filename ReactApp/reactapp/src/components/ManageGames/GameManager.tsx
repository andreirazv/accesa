import * as React from 'react';
import NavBarGame from './Navigation/NavBarGame';
import GamesList from './GamesList/GamesList';
import AddGame from './AddGame';
import Game from './GamesList/Game/Game';
export interface IGameManagerState {
    gameId?: string;
    selectedPage?: boolean;
}
export interface IGameManagerProps {
    isAllGames?: boolean;
}

export default class GameManager extends React.Component<IGameManagerProps, IGameManagerState> {

  constructor(props: IGameManagerProps) {
    super(props);
    this.state = {
        selectedPage:false
    }
  }
  public gameSelected = (gameId: string) => {
    this.setState({
      gameId: gameId
    });
  }
public handleChange=()=>
{
    this.setState({
        selectedPage: !this.state.selectedPage
      });
      
}
    public render()
    {
        let pageRender;
        if(this.state.selectedPage==false)
        {
            pageRender=<GamesList onGameSelected={this.gameSelected} selectGame={Game}/>
        }
        else
        {
            pageRender=<AddGame/>
        }
        return (
            <div>
                <NavBarGame handleChange={this.handleChange} selectActivePage={this.state.selectedPage}/>
                {pageRender}
                {!!this.state.gameId}
            </div>
        )
    }
}