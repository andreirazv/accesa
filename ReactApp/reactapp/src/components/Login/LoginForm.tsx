import * as React from 'react';
import './Login.scss';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
export interface ILoginFormProps {
    handleChange(e: any): void;
    loginButton(): void;
    responseFacebook(response:any): void;
    responseGoogle(response:any): void;
    registerButton(): void;
    name: string;
    pass: string;
}

const LoginForm = (props: ILoginFormProps) => {
    return (
      <div className = "bg-opacity">
  <div className="loginpanel">
  <div className="txt">
    <input type="text" name="name" placeholder="Username" onChange={props.handleChange} value={props.name}/><br /><br />
    <label htmlFor="user" className="entypo-user"></label>

  </div>
  <div className="txt">

    <input type="password" name="pass" placeholder="Password" maxLength={8} onChange={props.handleChange} value={props.pass}/><br /><br />
    <label htmlFor="pwd" className="entypo-lock"></label>

  </div>
  <div className="buttons">
    <input onClick={props.loginButton} type="button" value="Login" />
    <span>
      <a onClick={props.registerButton} className="entypo-user-add register">Register</a>
    </span>
  </div>
  
  <div className="hr">
    <div></div>
    <div>OR</div>
    <div></div>
  </div>
  
  <div className="social">

   <a> <GoogleLogin
        clientId="287859511961-ovcrvc0m41s7hpnj4h1mb6vtdudfcrcl.apps.googleusercontent.com"
        buttonText="Login"
        scope="https://www.googleapis.com/auth/cloud-platform https://www.googleapis.com/auth/cloud-vision"
        onSuccess={props.responseGoogle}
        onFailure={props.responseGoogle}/></a>
   <a> <FacebookLogin
      appId="297686470882191"
      buttonStyle={{height:'45px',width:'95px',textAlign:'left',fontSize:'12px',padding:'10px 10px 10px 10px'}}
      textButton={'Facebook'}
      autoLoad={false}
      fields="name,email,picture"
      onClick={props.responseFacebook}
      callback={props.responseFacebook} 
/></a>
    
  </div>
</div>
</div>
    );
    //     

}

export default LoginForm;