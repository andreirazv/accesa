import * as React from 'react';
import './Login.css';
import AuthService from '../../services/AuthService';
import RegisterForm from './RegisterForm';
import { IPlayer } from 'src/DataModel/IPlayer';
import PlayerService from 'src/services/PlayerService';
export interface IRegisterState {
    userName: string;
    password: string;
    nickName: string;
    Auth: AuthService;
    redirect: boolean;
}
export interface IRegisterProps {
    handleCloseModal():void;
    changeView():void;
   }
export class Register extends React.Component<IRegisterProps, IRegisterState> {
    constructor(props:IRegisterProps) {
        super(props);
        this.state = {
            userName: '',
            nickName: '',
            password: '',
            Auth : new AuthService(),
            redirect :false,
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }
    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: IRegisterState) => (
            { 
                ...prevState,
                [name]: value
            }
        ));
    }
    handleFormSubmit(){
        console.log("username:"+this.state.userName);
        console.log("nickname:"+this.state.nickName);
        console.log("password:"+this.state.password);
        const newPlayer:IPlayer={
            username:this.state.userName,
            playerName: this.state.nickName,
            password: this.state.password,
            totalStatsId: "",
            playerPhotoId: "",
        }
        PlayerService.insertPlayer(newPlayer).then((player:IPlayer)=>{
            this.state.Auth.login(player.username||'',player.password)
            .then((player:IPlayer) =>{
                this.state.Auth.setToken(player.token||'') 
                localStorage.setItem('logged_playerId',player._id||'') 
                history.pushState('','','/leaderboard');
                history.go();
              })
              .catch((err:any) =>{
                  alert("Username or password is wrong!");
              });
        }).catch((er:any)=>{
            alert("This user can`t be added!\n:"+er);
        });
    }


    public render() {
        return (
            
            <div>
                   <RegisterForm
                registerButton={()=>this.handleFormSubmit()}
                loginButton={()=>this.props.changeView()}
                nickName={this.state.nickName}
                userName={this.state.userName}
                password={this.state.password}
                handleChange={this.handleChange}/>
               
          </div>
        );
    }
}