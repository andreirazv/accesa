import * as React from 'react';
import './Login.scss';
export interface IRegisterFormProps {
    handleChange(e: any): void;
    registerButton(): void;
    loginButton(): void;
    userName: string;
    password: string;
    nickName: string;
}

const RegisterForm = (props: IRegisterFormProps) => {
    return (
      <div className = "bg-opacity">
  <div className="loginpanel">
  <div className="txt">
    <input type="text" name="nickName" placeholder="Nickname" onChange={props.handleChange} value={props.nickName}/><br /><br />
    <label htmlFor="nick" className="entypo-user"></label>

  </div>

  <div className="txt">
    <input type="text" name="userName" placeholder="Username" onChange={props.handleChange} value={props.userName}/><br /><br />
    <label htmlFor="user" className="entypo-user"></label>

  </div>
  <div className="txt">

    <input type="password" name="password" placeholder="Password" maxLength={8} onChange={props.handleChange} value={props.password}/><br /><br />
    <label htmlFor="pwd" className="entypo-lock"></label>

  </div>
  <div className="buttons">
    <input onClick={props.registerButton} type="button" value="Register" />
    <span>
      <a onClick={props.loginButton} className="entypo-user-add register">Login</a>
    </span>
  </div>
</div>
</div>
    );
    //     

}

export default RegisterForm;