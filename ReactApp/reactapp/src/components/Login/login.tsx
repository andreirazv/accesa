import * as React from 'react';
import './Login.css';
import AuthService from '../../services/AuthService';
import { IPlayer } from 'src/DataModel/IPlayer';
import PlayerService from 'src/services/PlayerService';
import LoginForm from './LoginForm';
export interface ILoginState {
    name: string;
    pass: string;
    Auth: AuthService;
    redirect: boolean;
    loginError: boolean;
    loginFacebook: boolean;
}
export interface ILoginProps {
    handleCloseModal():void;
    changeView():void;
   }
export class Login extends React.Component<ILoginProps, ILoginState> {
    constructor(props:ILoginProps) {
        super(props);
        this.state = {
            name: '',
            pass: '',
            Auth : new AuthService(),
            redirect :false,
            loginError: false,
            loginFacebook:false,
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.responseGoogle = this.responseGoogle.bind(this);
        this.signup = this.signup.bind(this);
    }
    handleChange = (e: any) => {
        const { name, value } = e.target;
        this.setState((prevState: ILoginState) => (
            { 
                ...prevState,
                [name]: value
            }
        ));
    }
    handleFormSubmit(){
        this.state.Auth.login(this.state.name,this.state.pass)
            .then((player:IPlayer) =>{
              this.state.Auth.setToken(player.token||'') 
              localStorage.setItem('logged_playerId',player._id||'') 
              history.pushState('','','/leaderboard');
              history.go();
            })
            .catch((err:any) =>{
                alert("Username or password is wrong!");
            });
    }
    loginUsingFacebook(){
        this.setState({loginFacebook:!this.state.loginFacebook})

    }
    responseGoogle = (response:any) => {
        this.signup(response,"google");
      }
      responseFacebook = (response:any) => {
        this.signup(response,"facebook");
      }
      componentClicked = (response:any) => {
      }
      signup(res:any,type:string)
      {
        if (type === 'facebook' && res.email) {
            PlayerService.getPlayerByName(res.name).then((player:IPlayer)=>{
                localStorage.setItem('logged_playerId',player._id||'');
                this.state.Auth.login(player.username||'',player.password)
                .then((player:IPlayer) =>{
                  this.state.Auth.setToken(player.token||'') 
                  history.pushState('','','/leaderboard');
                  history.go();
                }).catch((err:any) =>{
                    alert("Username or password is wrong!");
                });
            });
       }
       
       if (type === 'google' && res.w3.U3) {
        PlayerService.getPlayerByName(res.w3.ig).then((player:IPlayer)=>{
            localStorage.setItem('logged_playerId',player._id||'');
            localStorage.setItem('id_token_google',res.Zi.access_token||'');
            localStorage.setItem('id_token_google_exp',res.Zi.expires_at||'');
            this.state.Auth.login(player.username||'',player.password)
            .then((player:IPlayer) =>{
              this.state.Auth.setToken(player.token||'') 
              history.pushState('','','/leaderboard');
              history.go();
            }).catch((err:any) =>{
                alert("Username or password is wrong!");
            });
        }); 
       }
    }

    public render() {
        if (this.state.redirect || sessionStorage.getItem('userData')) {
           // return (<Redirect to={'/home'}/>)
        }
        
        return (
            
            <div>
                   <LoginForm
                responseGoogle={this.responseGoogle}
                responseFacebook={this.responseFacebook}
                registerButton={()=>this.props.changeView()}
                loginButton={()=>this.handleFormSubmit()}
                name={this.state.name}
                pass={this.state.pass}
                handleChange={this.handleChange}/>
               
          </div>
        );
    }
}