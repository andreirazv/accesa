import * as React from 'react';
import './Home.css';
import GamesList from '../ManageGames/GamesList/GamesList';
import Game from '../ManageGames/GamesList/Game/Game';
export interface IHomeState {
  gameId?: string;
}

export default class Homes extends React.Component<any, IHomeState> {

  constructor(props: any) {
    super(props);

    this.state = {
    }
  }

  public gameSelected = (gameId: string) => {
    this.setState({
      gameId: gameId
    });
  }

  public render() {
    return (
      <div className="home">
        <GamesList onGameSelected={this.gameSelected} selectGame={Game}/>
        {
          !!this.state.gameId
        }
      </div>
    )
  }
}