import * as React from 'react';
import './Home.css';
import BodyBeforeLogin from '../BeforeLogin/Body/BodyBeforeLogin';
import AuthService from '../../services/AuthService';

export interface IHomeState {
  Auth: AuthService;
}

export default class Home extends React.Component<any, IHomeState> {
  constructor(props: any) {
    super(props);
    this.state = {
      Auth:new AuthService(),
    }
  }

  componentWillMount() {
    if (this.state.Auth.loggedIn()) 
      this.props.history.push("/leaderboard");
    }
  public render()
  {
    if(this.state.Auth.loggedIn()==false)
    {
      return (
        <div>
          <BodyBeforeLogin/>
        </div>
      )
    }
    else return null;
  }
}