﻿using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.Services
{
    public class MatchService
    {
        private readonly IRepository<Match> _matchRepository;
        private readonly IRepository<Team> _teamRepository;
        public MatchService(IRepository<Match> matchRepository,IRepository<Team> teamRepository)
        {
            _matchRepository = matchRepository;
            _teamRepository = teamRepository;
        }
        public bool Update(Match result)
        {
            return _matchRepository.Update(result);
        }
        public bool Remove(ObjectId key)
        {
            return _matchRepository.Remove(key);
        }
        public IEnumerable<Match> FindAll()
        {
            return _matchRepository.FindAll();
        }
        public bool Insert(Match result)
        {
            return _matchRepository.Insert(result);
        }
        public Match FindById(ObjectId key)
        {
            return _matchRepository.FindById(key);
        }
        public int GetPointsByTeamId(ObjectId matchid, ObjectId teamid)
        {
            Match match = _matchRepository.FindById(matchid);
            int nr = int.Parse((match.TeamsAndPoints.ToList().Find(el =>el.Key.Equals(teamid.ToString())).Value).ToString());
            Debug.WriteLine(nr);
            return nr;
        }
        public int GetPointsByPlayerId(string matchid, string teamid,string playerId)
        {
            Match match = _matchRepository.FindById(ObjectId.Parse(matchid));
            int nr = int.Parse((match.TeamsAndPoints.ToList().Find(el => el.Key.Equals(teamid.ToString())).Value).ToString());
            Debug.WriteLine(nr);
            return nr;
        }
        public Match AddPoints(ObjectId matchid, ObjectId teamid,int points)
        {

            Match match = _matchRepository.FindById(matchid);
            int getPoints = int.Parse(match.TeamsAndPoints[teamid.ToString()].ToString());
            getPoints += points;
            match.TeamsAndPoints[teamid.ToString()] = getPoints;
            _matchRepository.Update(match);
            return _matchRepository.FindById(match._id);            
        }
        public Match AddTeams(ObjectId matchid, ObjectId teamid)
        {
            try
            {
                Match match = _matchRepository.FindById(matchid);
                match.TeamsAndPoints.Add(teamid.ToString(), 0);
                _matchRepository.Update(match);
                return match;
            }
            catch(Exception ex)
            { return null; }
            
        }
        public string GetWinner(string matchid)
        {
            try
            {
                Match match = _matchRepository.FindById(ObjectId.Parse(matchid));
                int max = 0;
                string id = "";
                match.TeamsAndPoints.ToList().ForEach(x =>
                {
                    if (int.Parse(x.Value.ToString()) >= max)
                           { max = int.Parse(x.Value.ToString());id = x.Key;
                        
                }
                });
                return id;
            }
            catch(Exception ex)
            { return null; }
            
        }
    }
}
