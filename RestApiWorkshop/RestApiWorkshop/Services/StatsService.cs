﻿using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.Services
{
    public class StatsService
    {
        private readonly IRepository<Result> _resultRepository;
        private readonly IRepository<Statistic> _statsRepository;
        public StatsService(IRepository<Result> resultRepository, IRepository<Statistic> statsRepository)
        {
            _resultRepository = resultRepository;
            _statsRepository = statsRepository;
        }
        public bool Update(Statistic statistic)
        {
            return _statsRepository.Update(statistic);
        }
        public bool Remove(ObjectId key)
        {
            return _statsRepository.Remove(key);
        }
        public IEnumerable<Statistic> FindAll()
        {
            return _statsRepository.FindAll();
        }
        public bool Insert(Statistic statistic)
        {
            return _statsRepository.Insert(statistic);
        }
        public Statistic FindById(ObjectId key)
        {
            return _statsRepository.FindById(key);
        }
        public Statistic FindStatsForPlayer(string gameId, string typeId)
        {
            Statistic statistic = new Statistic();
            _resultRepository.FindAll().ToList().ForEach(el =>
            {
                if (el.GameId.Equals(gameId) && el.TypeGameId.Equals(typeId))
                {
                    Statistic tempStats = _statsRepository.FindById(ObjectId.Parse(el.StatsId));
                    statistic.Wins += tempStats.Wins;
                    statistic.Deafeats += tempStats.Deafeats;
                    statistic.Draws += tempStats.Draws;
                }
            });
            return statistic;
        }
        public Statistic FindStatsForTeam(string gameId, string typeId, string teamId)
        {
            Result result= _resultRepository.FindAll().ToList().Find(el => el.GameId.Equals(gameId) && el.TypeGameId.Equals(typeId) && el.TeamId.Equals(teamId));
            Statistic statistic =_statsRepository.FindById(ObjectId.Parse(result.StatsId));
            return statistic;
        }
    }
}
