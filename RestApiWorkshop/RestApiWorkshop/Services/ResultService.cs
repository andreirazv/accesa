﻿using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.Services
{
    public class ResultService
    {
        private readonly IRepository<Result> _resultRepository;
        private readonly IRepository<Statistic> _statsRepository;
        public ResultService(IRepository<Result> resultRepository,IRepository<Statistic> statsRepository)
        {
            _resultRepository = resultRepository;
            _statsRepository = statsRepository;
        }
        public bool Update(Result result)
        {
            return _resultRepository.Update(result);
        }
        public bool Remove(ObjectId key)
        {
            return _resultRepository.Remove(key);
        }
        public IEnumerable<Result> FindAll()
        {
            return _resultRepository.FindAll();
        }
        public bool Insert(Result result)
        {
            return _resultRepository.Insert(result);
        }
        public Result FindById(ObjectId key)
        {
            return _resultRepository.FindById(key);
        }
 
    }
}