﻿using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.Services
{
    public class TeamService
    {
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<Player> _playerRepository;
        public TeamService(IRepository<Team> teamRepository,IRepository<Player> playerRepository)
        {
            _teamRepository = teamRepository;
            _playerRepository = playerRepository;
        }
        public bool Update(Team team)
        {
            return _teamRepository.Update(team);
        }
        public bool Remove(ObjectId key)
        {
            return _teamRepository.Remove(key);
        }
        public IEnumerable<Team> FindAll()
        {
            return _teamRepository.FindAll();
        }
        public bool Insert(Team team)
        {
            return _teamRepository.Insert(team);
        }
        public Team FindById(ObjectId key)
        {
            return _teamRepository.FindById(key);
        }
        public Team CheckTeamOrAddByPlayerId(string idPlayer)
        {
            try { _playerRepository.FindById(ObjectId.Parse(idPlayer)); }
            catch { return null; }
            Team team = default(Team);
            team = _teamRepository.FindAll().ToList().Find(el => el.Players.Count == 1&&el.Players.First().Equals(idPlayer));
            
            if (team == default(Team))
            {
                team = new Team
                {
                    TeamName = "",
                    Players = new List<string> { idPlayer }
                };
                _teamRepository.Insert(team);
            }
            return team;
        }
    }
}
