﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Helpers;
using RestApiWorkshop.Repository;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RestApiWorkshop.Services
{
    public interface IAuthService
    {
        Player Authenticate(string username, string password);
        IEnumerable<Player> GetAll();
    }

    public class AuthService : IAuthService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        // new List<Player>
        /*{
            new Player { Id = 1, FirstName = "Test", LastName = "User", Username = "test", Password = "test" }
        };
        */
        private readonly AppSettings _appSettings;
        private readonly IRepository<Player> _playerRepository;
        private List<Player> _users;
        public AuthService(IRepository<Player> playerRepository, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            _playerRepository = playerRepository;
            _users = _playerRepository.FindAll().ToList();
        }
        
        public Player Authenticate(string username, string password)
        {
            var user = _users.SingleOrDefault(x => x.Username == username && x.Password == password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user._id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }

        public IEnumerable<Player> GetAll()
        {
            // return users without passwords
            return _users.Select(x => {
                x.Password = null;
                return x;
            });
        }
    }
}
