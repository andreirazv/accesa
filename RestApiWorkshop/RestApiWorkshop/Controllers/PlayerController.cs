﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.DataLayer.DataModel;
using RestApiWorkshop.Repository;
using RestApiWorkshop.Services;

namespace RestApiWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class PlayerController : ControllerBase
    {
        private readonly PlayerService _playerService;
        private readonly IRepository<Statistic> _statsRepository;
        
        public PlayerController(PlayerService playerService, IRepository<Statistic> statsRepository)
        {
            _playerService = playerService;
            _statsRepository = statsRepository;
        }
        [HttpGet]
        public IEnumerable<Player> GetAll()
        {
            return _playerService.FindAll();

        }
        [HttpGet("pagg&take={pagesize}&skip={skippage}", Name = "GetRankPagg")]
        public ActionResult GetRankPagg(string pagesize, string skippage)
        {
            List<Status> rankList = new List<Status>();
            try
            {
                
                List<Player> asList = GetAll().ToList();
                int szPage = int.Parse(pagesize);
                int skPage = int.Parse(skippage);
                asList.ForEach(player =>
                {
                    if (player.TotalStatsId != null)
                    {
                        Statistic Statistic = _statsRepository.FindById(ObjectId.Parse(player.TotalStatsId));
                        if (Statistic != null)
                        {
                            Status status = new Status { _id = ObjectId.GenerateNewId(), Name = player.PlayerName, Wins = Statistic.Wins, Defeats = Statistic.Deafeats, Draws = Statistic.Draws };
                            rankList.Add(status);
                        }
                    }
                });
                rankList = rankList.GetRange(skPage, szPage);
                rankList = rankList.OrderByDescending(item => item.Wins).ToList();
                int i = 1;
                rankList.ForEach(item =>
                {
                    item.Position = i;
                    i++;
                });
                return Ok(rankList.ToList());
            }
            catch
            {
                return BadRequest();
            }
        }
        [HttpGet("rank")]
        public IEnumerable<Status> GetRankPlayers()
        {
            List<Status> rankList=new List<Status>();
            List<Player> asList = GetAll().ToList();
           
            asList.ForEach(player =>
            {
                if (player.TotalStatsId != null)
                {
                    Statistic Statistic = _statsRepository.FindById(ObjectId.Parse(player.TotalStatsId));
                    if (Statistic != null)
                    {
                        Status status = new Status { _id = ObjectId.GenerateNewId(), Name = player.PlayerName, Wins = Statistic.Wins, Defeats = Statistic.Deafeats, Draws = Statistic.Draws };
                        rankList.Add(status);
                    }
                }
            });

            rankList = rankList.OrderByDescending(item => item.Wins).ToList();
            int i = 1;
            rankList.ForEach(item => {
                item.Position = i;
                i ++;
            });
            return rankList.ToList();
        }
        [HttpGet("desc")]
        public IEnumerable<Player> GetDesc()
        {
            return _playerService.FindAll().OrderByDescending(item => item.RegisterDate);

        }
        [HttpGet("id/{id}", Name = "GetPlayerById")]
        public Player GetById(string id)
        {
            var player = default(Player);
            if (!string.IsNullOrEmpty(id))
            {
                player = _playerService.FindById(ObjectId.Parse(id));
            }
            return player;
        }
        [HttpGet("name/{name}", Name = "getPlayerByName")]
        public Player GetPlayerByName(string name)
        {
            var player = default(Player);
            if (!string.IsNullOrEmpty(name))
            {
                player = _playerService.GetPlayerByName(name);
            }
            return player;
        }

        [HttpPost]
        public ActionResult Post([FromBody] Player player)
        {

            var result = string.Empty;
            var gameinDb = _playerService.FindById(player._id);
            if (gameinDb == null)
            {
                _playerService.Insert(player);
                return Ok(player);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Player value)
        {
            if (!string.IsNullOrEmpty(id))
            {
                Player temp = _playerService.FindById(ObjectId.Parse(id));
                if (temp != null)
                {
                    if (temp._id != ObjectId.Empty)
                    {
                        _playerService.Update(value);
                        return Ok();
                    }
                    else
                        return BadRequest();
                }
                return BadRequest();
            }
            return BadRequest();
        }

        [HttpDelete("{name}", Name = "DeletePlayerById")]
        public ActionResult DeleteById(string id)
        {
            var player = default(Player);
            if (!string.IsNullOrEmpty(id))
            {
                player = _playerService.FindById(ObjectId.Parse(id));
                if (player != null)
                {
                    _playerService.Remove(player._id);
                    return Ok();
                }
            }
            return BadRequest();
        }
    }
}
