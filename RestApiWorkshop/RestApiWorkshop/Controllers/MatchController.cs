﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;
using RestApiWorkshop.Services;

namespace RestApiWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class MatchController : ControllerBase
    {
        private readonly MatchService _matchService;
        public MatchController(MatchService matchService, IRepository<Game> gameRepository)
        {
            _matchService = matchService;
        }
        [HttpGet]
        public IEnumerable<Match> GetAll()
        {
            return _matchService.FindAll();

        }
        [HttpGet("{id}", Name = "getMatchById")]
        public ActionResult GetMatchById(string id)
        {
            var match = default(Match);
            if (!string.IsNullOrEmpty(id))
            {
                try
                {
                    match = _matchService.FindById(ObjectId.Parse(id));
                    
                }
                catch ( Exception ex)
                {
                    BadRequest(ex);
                }
            }
            return Ok(match);
        }
        [HttpGet("getpoints&matchid={matchid}&teamid={teamid}", Name = "GetPointsByTeamId")]
        public ActionResult GetPointsByTeamId(string matchid, string teamid)
        {
            Debug.WriteLine(matchid);
            Debug.WriteLine(teamid);
            if ((!string.IsNullOrEmpty(matchid))&& (!string.IsNullOrEmpty(teamid)))
            {
                int points = _matchService.GetPointsByTeamId(ObjectId.Parse(matchid),ObjectId.Parse(teamid));
                Debug.WriteLine(points);
                return Ok(points);
            }
            return BadRequest();
        }
        [HttpGet("getplayerpoints&matchid={matchid}&teamid={teamid}&playerid={playerid}", Name = "GetPointsByPlayerId")]
        public ActionResult GetPointsByPlayerId(string matchid, string teamid,string playerid)
        {
            if ((!string.IsNullOrEmpty(matchid))&& (!string.IsNullOrEmpty(teamid)))
            {
                int points = _matchService.GetPointsByPlayerId(matchid,teamid,playerid);
                return Ok(points);
            }
            return BadRequest();
        }
        [HttpGet("getwinner&match={matchid}", Name = "GetWinner")]
        public ActionResult GetWinner(string matchid)
        {
            Debug.WriteLine(matchid);
            if (!string.IsNullOrEmpty(matchid))
            {
                string team = _matchService.GetWinner(matchid);
                return Ok(team);
            }
            return BadRequest();
        }
        [HttpGet("matchid={matchid}&teamid={teamid}&points={points}", Name = "AddPoints")]
        public ActionResult AddPoints(string matchid, string teamid,string points)
        {
            Match match = default(Match);

            if ((!string.IsNullOrEmpty(matchid))&& (!string.IsNullOrEmpty(teamid))&& (!string.IsNullOrEmpty(points)))
            {
                match = _matchService.AddPoints(ObjectId.Parse(matchid),ObjectId.Parse(teamid),int.Parse(points));
               return Ok(match);
            }
            return BadRequest(match);
        }
        [HttpPost(Name ="Insert")]
        public ActionResult Post([FromBody] Match match)
        {
            var result = string.Empty;

            var matchInDb = _matchService.FindById(match._id);
            if (matchInDb == null)
            {
                _matchService.Insert(match);
                return Ok(match);
            }
            else
            {
                return Ok(matchInDb);
            }
        }
        [HttpGet("add&matchid={matchId}&teamid={teamid}", Name = "teamlist")]
        public ActionResult AddTeamList(string matchId, string teamid)
        {
            Debug.WriteLine(matchId);
            Debug.WriteLine(teamid);
            Match match = default(Match);
            
            if ((!string.IsNullOrEmpty(matchId)) && (!string.IsNullOrEmpty(teamid)))
            {
                try
                {
                    match = _matchService.AddTeams(ObjectId.Parse(matchId), ObjectId.Parse(teamid));
                    return Ok(match);
                }
                catch(Exception ex){ }
            }
            return BadRequest();
        }
        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Match value)
        {
            if (!string.IsNullOrEmpty(id))
            {
                Debug.WriteLine(value._id);
                Match temp = _matchService.FindById(ObjectId.Parse(id));
                if (temp != null)
                {
                    if (temp._id != ObjectId.Empty)
                    {
                        value._id = ObjectId.Parse(id);
                        _matchService.Update(value);
                        Debug.WriteLine(value._id);
                        return Ok(_matchService.FindById(value._id));
                    }
                    else
                        return BadRequest(value);
                }
                return BadRequest(value);
            }
            return BadRequest(value);
        }

        [HttpDelete("{idString}", Name = "DeleteMatch")]
        public ActionResult DeleteByID(string idString)
        {
            ObjectId id=ObjectId.Empty;
            try
            {
                id = ObjectId.Parse(idString);
                var match = default(Match);
                if (ObjectId.Empty != id)
                {
                    match = _matchService.FindById(id);
                    if (match != null)
                    {
                        _matchService.Remove(match._id);
                        return Ok(match);
                    }
                }
            }
            catch
            {
                return BadRequest();
            }
            return BadRequest();
        }
    }
}
