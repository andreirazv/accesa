﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;
using RestApiWorkshop.Services;

namespace RestApiWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class ResultController : ControllerBase
    {
        private readonly ResultService _resultService;
        public ResultController(ResultService resultService)
        {
            _resultService = resultService;
        }
        [HttpGet]
        public IEnumerable<Result> GetAll()
        {
            return _resultService.FindAll();
        }
        [HttpGet("desc/date")]
        public IEnumerable<Result> GetDesc()
        {
            return _resultService.FindAll().OrderByDescending(item => item.Date);

        }

        [HttpPost]
        public ActionResult Post([FromBody] Result Result)
        {
            var result = string.Empty;
            var existGame = _resultService.FindById(Result._id);
            if (existGame == null)
            {
                _resultService.Insert(Result);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Result value)
        {
            if (!string.IsNullOrEmpty(id))
            {
                Result temp = _resultService.FindById(ObjectId.Parse(id));
                if (temp != null)
                {
                    if (temp._id != ObjectId.Empty)
                    {
                        _resultService.Update(value);
                        return Ok();
                    }
                    else
                        return BadRequest();
                }
                return BadRequest();
            }
            return BadRequest();
        }
        [HttpGet("id/{id}", Name = "getResulthById")]
        public Result GetResultById(string id)
        {
            var result = default(Result);
            if (!string.IsNullOrEmpty(id))
            {
                result = _resultService.FindById(ObjectId.Parse(id));
            }
            return result;
        }
        [HttpDelete("{idString}", Name = "DeleteResult")]
        public ActionResult Delete(string idString)
        {
            ObjectId id = ObjectId.Empty;
            try
            {
                id = ObjectId.Parse(idString);
                var result = default(Result);
                if (ObjectId.Empty != id)
                {
                    result = _resultService.FindById(id);
                    if (result != null)
                    {
                        _resultService.Remove(result._id);
                        return Ok();
                    }
                }
            }
            catch
            {
                return BadRequest();
            }
            return BadRequest();
        }
    }
}
