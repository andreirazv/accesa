﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;
using RestApiWorkshop.Services;

namespace RestApiWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class StatsController : ControllerBase
    {
        private readonly StatsService _statsService;
        public StatsController(StatsService statsService)
        {
            _statsService = statsService;
        }
        [HttpGet]
        public IEnumerable<Statistic> GetAll()
        {
            return _statsService.FindAll();

        }
        [HttpGet("id/{id}", Name = "GetStatsById")]
        public ActionResult GetStatsById(string id)
        {
            var stats = default(Statistic);
            if (!string.IsNullOrEmpty(id))
            {
                try
                {
                    stats = _statsService.FindById(ObjectId.Parse(id));
                    return Ok(stats);
                }
                catch(Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return NotFound();
        }
        [HttpGet("desc/win")]
        public IEnumerable<Statistic> GetDescWin()
        {
            return _statsService.FindAll().OrderByDescending(item => item.Wins);
        }
        [HttpGet("desc/defeat")]
        public IEnumerable<Statistic> GetDescDefeat()
        {
            return _statsService.FindAll().OrderByDescending(item => item.Deafeats);
        }
        [HttpGet("desc/draw")]
        public IEnumerable<Statistic> GetDescDraw()
        {
            return _statsService.FindAll().OrderByDescending(item => item.Draws);
        }

        [HttpGet("statsplayer/gameid={idGame}&&typeid={idType}", Name = "GetStatsForPlayer")]
        public Statistic GetStatsForPlayer(string idGame,string idType)
        {
            var stats = default(Statistic);
            if ((!string.IsNullOrEmpty(idGame)) && (!string.IsNullOrEmpty(idType)))
            {
                stats = _statsService.FindStatsForPlayer(idGame, idType);
            }
            return stats;
        }
        [HttpGet("statsteam/gameid={idGame}&&typeid={idType}&&teamid={idTeam}", Name = "GetStatsForTeam")]
        public Statistic GetStatsForTeam(string idGame,string idType, string idTeam)
        {
            var stats = default(Statistic);
            if ((!string.IsNullOrEmpty(idGame))&& (!string.IsNullOrEmpty(idType))&& (!string.IsNullOrEmpty(idTeam)))
            {
                stats = _statsService.FindStatsForTeam(idGame, idType, idTeam);
            }
            return stats;
        }
        [HttpPost]
        public ActionResult Post([FromBody] Statistic Statistic)
        {
            var result = string.Empty;
            var existGame = _statsService.FindById(Statistic._id);
            if (existGame == null)
            {
                _statsService.Insert(Statistic);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Statistic value)
        {
            if (!string.IsNullOrEmpty(id))
            {
                Statistic temp = _statsService.FindById(ObjectId.Parse(id));
                if (temp != null)
                {
                    if (temp._id != ObjectId.Empty)
                    {
                        _statsService.Update(value);
                        return Ok();
                    }
                    else
                        return BadRequest();
                }
                return BadRequest();
            }
            return BadRequest();
        }

        [HttpDelete("{idString}", Name = "DeleteStats")]
        public ActionResult DeleteByID(string idString)
        {
            ObjectId id = ObjectId.Empty;
            try
            {
                id = ObjectId.Parse(idString);
                var Statistic = default(Statistic);
                if (ObjectId.Empty != id)
                {
                    Statistic = _statsService.FindById(id);
                    if (Statistic != null)
                    {
                        _statsService.Remove(Statistic._id);
                        return Ok();
                    }
                }
            }
            catch
            {
                return BadRequest();
            }
            return BadRequest();
        }
    }
}
