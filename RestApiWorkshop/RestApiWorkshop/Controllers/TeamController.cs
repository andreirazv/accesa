﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;
using RestApiWorkshop.Services;

namespace RestApiWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class TeamController : ControllerBase
    {
        private readonly TeamService _teamService;
        public TeamController(TeamService teamService)
        {
            _teamService = teamService;
        }
        [HttpGet]
        public IEnumerable<Team> GetAll()
        {
            return _teamService.FindAll();

        }
        [HttpGet("desc")]
        public IEnumerable<Team> GetDesc()
        {
            return _teamService.FindAll().OrderByDescending(item => item.Players.Count);

        }

        [HttpPost]
        public ActionResult Post([FromBody] Team team)
        {
            var result = string.Empty;
            var existGame = _teamService.FindById(team._id);
            if (existGame == null)
            {
                _teamService.Insert(team);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
           
        }
        [HttpGet("id/{id}", Name = "GetTeamById")]
        public ActionResult GetTeamById(string id)
        {
            var team = default(Team);
            if (!string.IsNullOrEmpty(id))
            {
                try
                {
                    team = _teamService.FindById(ObjectId.Parse(id));
                }
                catch(Exception ex)
                {
                    return NoContent();
                }
            }
            return Ok(team);
        }
        [HttpGet("playerid={id}", Name = "GetTeamByPlayerId")]
        public ActionResult GetTeamByPlayerId(string id)
        {
            var team = default(Team);
            if (!string.IsNullOrEmpty(id))
            {
                team = _teamService.CheckTeamOrAddByPlayerId(id);
            }
            if (team != null)
                return Ok(team);
            else
                return BadRequest();
        }
        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Team value)
        {
            if (!string.IsNullOrEmpty(id))
            {
                Team temp = _teamService.FindById(ObjectId.Parse(id));
                if (temp != null)
                {
                    if (temp._id != ObjectId.Empty)
                    {
                        _teamService.Update(value);
                        return Ok(_teamService.FindById(value._id));
                    }
                    else
                        return BadRequest(_teamService.FindById(value._id));
                }
                return BadRequest(_teamService.FindById(value._id));
            }
            return BadRequest(_teamService.FindById(value._id));
        }

        [HttpDelete("{id}", Name = "DeleteTeamById")]
        public ActionResult DeleteById(string id)
        {
            var team = default(Team);
            if (!string.IsNullOrEmpty(id))
            {
                team = _teamService.FindById(ObjectId.Parse(id));
                if (team != null)
                {
                    _teamService.Remove(team._id);
                    return Ok();
                }
            }
            return BadRequest();
        }
    }
}
