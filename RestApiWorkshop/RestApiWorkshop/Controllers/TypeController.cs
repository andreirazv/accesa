﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;

namespace RestApiWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class TypeController : ControllerBase
    {
        private readonly IRepository<TypeGame> _gameTypeRepository;
        public TypeController(IRepository<TypeGame> gameTypeRepository)
        {
            _gameTypeRepository = gameTypeRepository;
        }
        [HttpGet]
        public IEnumerable<TypeGame> GetAll()
        {
            return _gameTypeRepository.FindAll();

        }
        [HttpGet("desc")]
        public IEnumerable<TypeGame> GetDesc()
        {
            return _gameTypeRepository.FindAll().OrderByDescending(item => item.CountPlayed);

        }

        [HttpGet("id/{id}", Name = "TypeById")]
        public TypeGame GetTypeById(string id)
        {
            var gameType = default(TypeGame);
            if (!string.IsNullOrEmpty(id))
            {
                gameType = _gameTypeRepository.FindById(ObjectId.Parse(id));
            }
            return gameType;
        }
        [HttpPost]
        public ActionResult Post([FromBody] TypeGame type)
        {
            var result = string.Empty;
            var existGame = _gameTypeRepository.FindById(type._id);
            if (existGame == null)
            {
                _gameTypeRepository.Insert(type);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] TypeGame value)
        {
            if (!string.IsNullOrEmpty(id))
            {
                TypeGame temp = _gameTypeRepository.FindById(ObjectId.Parse(id));
                if (temp != null)
                {
                    if (temp._id != ObjectId.Empty)
                    {
                        _gameTypeRepository.Update(value);
                        return Ok();
                    }
                    else
                        return BadRequest();
                }
                return BadRequest();
            }
            return BadRequest();
        }

        [HttpDelete("{id}", Name = "DeleteTypeGame")]
        public ActionResult DeleteById(string id)
        {
            var type = default(TypeGame);
            if (!string.IsNullOrEmpty(id))
            {
                type = _gameTypeRepository.FindById(ObjectId.Parse(id));
                if (type != null)
                {
                    _gameTypeRepository.Remove(type._id);
                    return Ok();
                }
            }
            return BadRequest();
        }
    }
}
