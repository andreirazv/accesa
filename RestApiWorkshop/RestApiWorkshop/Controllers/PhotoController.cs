﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using System.Net.Http;
using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;
using System.Net;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Specialized;
using RestApiWorkshop.DataLayer.DataModel;

namespace RestApiWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class PhotoController : ControllerBase
    {
        private readonly IRepository<Photo> _photoRepository;
        public PhotoController(IRepository<Photo> photoRepository)
        {
            _photoRepository = photoRepository;
        }
        [HttpGet]
        public IEnumerable<Photo> GetAll()
        {
            return _photoRepository.FindAll();
        }
        [HttpGet("id/{id}", Name = "GetPhotoId")]
        public ActionResult GetById(string id)
        {
            var photo = default(Photo);
            if (!string.IsNullOrEmpty(id))
            {
                try
                {
                    photo = _photoRepository.FindById(ObjectId.Parse(id));
                }
                catch(Exception ex)
                {
                    return NoContent();
                }
            }
            return Ok(photo);
        }

        public async Task<string> Asd(string imageBase64,string token)
        {
            string val = await Task.Run(() => Ab(imageBase64, token));
            return val;
        }
        [HttpPost("img64", Name = "imageRecognition")]
        public ActionResult RecoginitionImage([FromBody]Request request)
        {
            string val = Asd(request.ImageBase64, request.Token).Result;
            return Ok(val);
            
        }
        public static async Task<string> Ab(string base64string,string token)
        {
            HttpResponseMessage response;
            var client = new HttpClient();
            
                string myJson = $@"{{
                    ""payload"":
                        {{
                            ""image"":{{
                                ""imageBytes"": ""{base64string}""
                            }}
                        }}
                }}";

                string requestUri = "https://automl.googleapis.com/v1beta1/projects/accesa-image/locations/us-central1/models/ICN5773494638050774217:predict?access_token=";
                requestUri += token;
                response = await client.PostAsync(
                    requestUri,
                    new StringContent(myJson, Encoding.UTF8, "application/json"));

                var responseStr = await response.Content.ReadAsStringAsync();

            return responseStr.ToString();
        }
        [HttpPost(Name ="GetPhoto")]
        public ActionResult Post([FromBody] Photo photo)
        {
            var result = string.Empty;
            var existGame = _photoRepository.FindById(photo._id);
            if (existGame == null)
            {
                _photoRepository.Insert(photo);
                return Ok(_photoRepository.FindById(photo._id));
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Photo value)
        {
            if (!string.IsNullOrEmpty(id))
            {
                Photo temp = _photoRepository.FindById(ObjectId.Parse(id));
                if (temp != null)
                {
                    if (temp._id != ObjectId.Empty)
                    {
                        _photoRepository.Update(value);
                        return Ok();
                    }
                    else
                        return BadRequest();
                }
                return BadRequest();
            }
            return BadRequest();
        }

        [HttpDelete("{idString}", Name = "DeletePhotoById")]
        public ActionResult DeleteById(string idString)
        {
            ObjectId id = ObjectId.Empty;
            try
            {
                id = ObjectId.Parse(idString);
                var photo = default(Photo);
                if (ObjectId.Empty != id)
                {
                    photo = _photoRepository.FindById(id);
                    if (photo != null)
                    {
                        _photoRepository.Remove(photo._id);
                        return Ok();
                    }
                }
            }
            catch
            {
                return BadRequest();
            }
            return BadRequest();
        }
    }
}
