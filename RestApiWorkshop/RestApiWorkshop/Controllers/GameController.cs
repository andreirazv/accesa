﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using RestApiWorkshop.DataLayer;
using RestApiWorkshop.Repository;

namespace RestApiWorkshop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class GameController : ControllerBase
    {
        private readonly IRepository<Game> _gameRepository;
        public GameController(IRepository<Game> gameRepository)
        {
            _gameRepository = gameRepository;
        }
        [HttpGet]
        public IEnumerable<Game> GetAll()
        {
            return _gameRepository.FindAll();
                
        }
        [HttpGet("desc")]
        public IEnumerable<Game> GetDesc()
        {
            return _gameRepository.FindAll().OrderByDescending(item=>item.CountPlayed);
        }

        [HttpGet("id/{id}", Name = "GetGameById")]
        public ActionResult GetGameById(string id)
        {
            var game = default(Game);
            if (!string.IsNullOrEmpty(id))
            {
                game = _gameRepository.FindById(ObjectId.Parse(id));
            }
            return Ok(game);
        }

        [HttpPost]
        [EnableCors("CorsPolicy")]
        public ActionResult Post([FromBody] Game game)
        {
            var result = string.Empty;
            var existGame = _gameRepository.FindById(game._id);
            bool findGame = default(bool);
            if(existGame==null)
                findGame=_gameRepository.Insert(game);
            if (findGame == false || game == null)
                return BadRequest();
            if(findGame==true)
                return Ok();
            else
                return BadRequest();
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, [FromBody] Game value)
        {
            if (!string.IsNullOrEmpty(id))
            {
                Game temp = _gameRepository.FindById(ObjectId.Parse(id));
                if (temp != null)
                {
                    if (temp._id != ObjectId.Empty)
                    {
                        _gameRepository.Update(value);
                        return Ok();
                    }
                    else
                        return BadRequest();
                }
                return BadRequest();
            }
            return BadRequest();
        }

        [HttpDelete("{id}", Name = "DeleteGame")]
        public ActionResult DeleteById(string id)
        {
            var game = default(Game);
            if (!string.IsNullOrEmpty(id))
            {
                game = _gameRepository.FindById(ObjectId.Parse(id));
                if (game!=null)
                {
                    _gameRepository.Remove(ObjectId.Parse(id));
                    return Ok();
                }    
            }
            return BadRequest();
        }
    }
}
