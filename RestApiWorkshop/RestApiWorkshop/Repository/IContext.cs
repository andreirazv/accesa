﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.Repository
{
    interface IContext<T>
    {
        IMongoCollection<T> Entities { get; set; }
    }
}
