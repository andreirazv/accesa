﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using RestApiWorkshop.DataLayer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.Repository
{
    
    class Context<T> : IContext<T>
    {
        private readonly IMongoDatabase _database;
        public Context(IOptions<Settings> options)
        {
            var client = new MongoClient(options.Value.ConnectionString);
            _database = client.GetDatabase(options.Value.DatabaseName);
        }

        public IMongoCollection<T> Entities
        {
            get
            {
                return _database.GetCollection<T>(typeof(T).Name);
            }
            set => throw new NotImplementedException();
        }

    }
    
}
