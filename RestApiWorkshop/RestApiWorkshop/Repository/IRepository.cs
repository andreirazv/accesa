﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.Repository
{
    public interface IRepository<T>
    {
        bool Insert(T obj);
        T FindById(ObjectId key);
        IEnumerable<T> FindAll();
        bool Update(T obj);
        bool Remove(ObjectId key);
    }
}
