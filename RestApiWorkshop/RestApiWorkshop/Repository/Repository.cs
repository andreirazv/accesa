﻿using MongoDB.Bson;
using MongoDB.Driver;
using RestApiWorkshop.DataLayer.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.Repository
{
    class Repository<T> : IRepository<T> where T : IHasID
    {
        private readonly IContext<T> _context;

        public Repository(IContext<T> context)
        {
            _context = context;
        }

        public bool Remove(ObjectId key)
        {
            var filter = Builders<T>.Filter.Eq<ObjectId>(g => g._id, key);
            if (_context.Entities.DeleteOne(filter).DeletedCount == 1)
                return true;
            return false;
        }

        public IEnumerable<T> FindAll()
        {
            var emptyFilter = Builders<T>.Filter.Empty;
            return _context.Entities.FindSync(emptyFilter).ToList();
        }

        public T FindById(ObjectId key)
        {
            var filter = Builders<T>.Filter.Eq<ObjectId>(g => g._id, key);
            return _context.Entities.FindSync(filter).FirstOrDefault();
        }

        public bool Insert(T obj)
        {
            try
            {
                _context.Entities.InsertOne(obj);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(T obj)
        {
            var filter = Builders<T>.Filter.Eq(u => u._id, obj._id);
            if (_context.Entities.ReplaceOne(filter, obj).ModifiedCount == 1)
                return true;

            return false;
        }
    }
}
