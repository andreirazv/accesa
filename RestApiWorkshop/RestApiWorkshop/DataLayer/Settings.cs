﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.DataLayer
{
    public class Settings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
