﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using RestApiWorkshop.DataLayer.DataModel;

namespace RestApiWorkshop.DataLayer
{
    public class Game:IHasID
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public string GameName { get; set; }
        public string GameRules { get; set; }
        public int CountPlayed { get; set; }
        public string GamePhotoId { get; set; }
    }
}
