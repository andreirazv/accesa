﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using RestApiWorkshop.DataLayer.DataModel;

namespace RestApiWorkshop.DataLayer
{
    public class Result : IHasID
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public string TeamId { get; set; }
        public string GameId { get; set; }
        public string TypeGameId { get; set; }
        public string StatsId { get; set; }
        public DateTime Date { get; set; }


    }
}
