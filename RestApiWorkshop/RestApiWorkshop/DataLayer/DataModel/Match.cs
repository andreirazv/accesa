﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using RestApiWorkshop.DataLayer.DataModel;

namespace RestApiWorkshop.DataLayer
{
    public class Match:IHasID
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }      
        public string GameId { get; set; }
        public string TypeGameId { get; set; }
        [BsonDictionaryOptions(DictionaryRepresentation.ArrayOfArrays)]
        public Dictionary<string, object> TeamsAndPoints { get; set; }
        public string WinnerTeam { get; set; }
        public string FinalPhotoId { get; set; }
        public DateTime DateEndMatch { get; set; }
    }
}
