﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using RestApiWorkshop.DataLayer.DataModel;

namespace RestApiWorkshop.DataLayer
{
    public class Photo : IHasID
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public string SourcePhoto { get; set; }

    }
}
