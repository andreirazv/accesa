﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.DataLayer.DataModel
{
    interface IHasID
    {
        ObjectId _id { get; set; }
    }
}
