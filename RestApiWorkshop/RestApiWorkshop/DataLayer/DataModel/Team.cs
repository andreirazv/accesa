﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using RestApiWorkshop.DataLayer.DataModel;

namespace RestApiWorkshop.DataLayer
{
    public class Team : IHasID
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public string TeamName { get; set; }
        public List<string> Players { get; set; }
        public string PhotoId { get; set; }
    }
}
