﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestApiWorkshop.DataLayer.DataModel;

namespace RestApiWorkshop.DataLayer.DataModel
{
    public class Status : IHasID
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public int Position { get; set; }
        public string Name { get; set; }
        public int Wins { get; set; }
        public int Defeats { get; set; }
        public int Draws { get; set; }
  
    }
}
