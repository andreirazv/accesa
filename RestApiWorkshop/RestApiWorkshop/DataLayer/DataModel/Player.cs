﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using RestApiWorkshop.DataLayer.DataModel;

namespace RestApiWorkshop.DataLayer
{
    public class Player : IHasID
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public string Username { get; set; }
        public string PlayerName { get; set; }
        public string Password { get; set; }
        public string TotalStatsId { get; set; }
        public string PlayerPhotoId { get; set; }
        public DateTime RegisterDate { get; set; }
        public string Token { get; set; }

    }
}
