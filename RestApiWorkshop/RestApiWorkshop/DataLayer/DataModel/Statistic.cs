﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using RestApiWorkshop.DataLayer.DataModel;

namespace RestApiWorkshop.DataLayer
{
    public class Statistic : IHasID
    {
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public int Wins { get; set; }
        public int Deafeats { get; set; }
        public int Draws { get; set; }

    }
}
