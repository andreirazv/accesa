﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiWorkshop.DataLayer.DataModel
{
    public class Request
    {
        public string ImageBase64 { get; set; }
        public string Token { get; set; }
    }
}
