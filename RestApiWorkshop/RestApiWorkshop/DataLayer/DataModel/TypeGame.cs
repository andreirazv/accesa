﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using RestApiWorkshop.DataLayer.DataModel;

namespace RestApiWorkshop.DataLayer
{
    public class TypeGame:IHasID
    {
        
        [BsonIgnoreIfDefault]
        public ObjectId _id { get; set; }
        public string TypeName { get; set; }
        public bool AllowTeam { get; set; }
        public int RemainingPoints { get; set; }
        public int Time { get; set; }
        public int Rounds { get; set; }
        public int CountPlayed { get; set; }
        public string PhotoId { get; set; }
    }
}
